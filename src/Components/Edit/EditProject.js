import React, { useState } from "react";
import "./Edit.css";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import Notiflix from "notiflix";
import {Select, MenuItem} from "@material-ui/core";
import moment from "moment";

function EditProject(props) {
  const { row } = props;
  const [projectName, setprojectName] = useState(row.project);
  const [client, setclient] = useState(row.client);
  const [managerAsigned, setmanagerAsigned] = useState(row.manager);
  const [dueDate, setdueDate] = useState(row.duedate);
  const [creationDate, setcreationDate] = useState(row.creationdate);
  const [closingDate, setclosingDate] = useState(row.closingdate);
  const [hourSpent, sethourSpent] = useState(row.hoursspent);
  const [costSpent, setcostSpent] = useState(row.costspent);
  const [status, setstatus] = useState(row.status);
  const [Billable, setBillable] = useState(row.billable);
  const projectInput = (e) => {
    setprojectName(e.target.value);
  };
  const clientInput = (e) => {
    setclient(e.target.value);
  };
  const managerAsignedInput = (e) => {
    setmanagerAsigned(e.target.value);
  };
  const dueDateInput = (e) => {
    setdueDate(e.target.value);
  };
  const creationDateInput = (e) => {
    setcreationDate(e.target.value);
  };
  const closingDateInput = (e) => {
    setclosingDate(e.target.value);
  };
  const hourSpentInput = (e) => {
    sethourSpent(e.target.value);
  };
  const costSpentInput = (e) => {
    setcostSpent(e.target.value);
  };
  const statusInput = (e) => {
    setstatus(e.target.value);
  };

  const BillableInput = (event) => {
    event.target.value == "true" ? setBillable(true) : setBillable(false);
  };

  const submitHandler = () => {
    var DUEDATE = moment(dueDate).format("YYYY-MM-DD");
    axios
      .post("http://139.59.73.146:9090/v1/api/update/project", {
        id: row.id,
        name: projectName,
        client: client,
        billable: Billable,
        manager_assigned: managerAsigned,
        status: status,
        due_date: DUEDATE,
      })
      .then((res) => {
        if (res.data.status == "UPDATED") {
          Notiflix.Notify.success("Project updated sucessfully");
          window.location.reload();
        } else {
          Notiflix.Notify.warning(res.data.status);
        }
      });
  };

  const resetHandler = () => {
    setprojectName("");
    setclient("");
    setmanagerAsigned("");
    setdueDate("");
    sethourSpent("");
    setcostSpent("");
    setstatus("");
    setBillable("");
    setclosingDate("");
    setcreationDate("");
  };

  const cancelHandler = () => {
    props.modalClosed();
  };

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">Edit Project</p>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <p className="Label">Project</p>
        <input
          className="inputField"
          autoFocus
          value={projectName}
          onChange={(e) => projectInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Client</p>
        <input
          className="inputField"
          autoFocus
          value={client}
          onChange={(e) => clientInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label" style={{ flex: "1" }}>
          Billable
        </p>
        <RadioGroup
          aria-label="gender"
          name="gender1"
          value={Billable}
          onChange={BillableInput}
          style={{ flex: "2.3", flexDirection: "row" }}
        >
          <FormControlLabel
            value={true}
            style={{ paddingBottom: "12px" }}
            control={<Radio />}
            label="True"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value={false}
            control={<Radio />}
            label="False"
          />
        </RadioGroup>
      </div>
      <div className="row">
        <p className="Label">Manager Assigned</p>
        <Select
          className="inputField"
          autoFocus
          value={managerAsigned}
          onChange={(e) => managerAsignedInput(e)}
        >
          {props.managerList.map((manager)=>{
            return <MenuItem value={manager}>{manager}</MenuItem>
          })}
        </Select>
      </div>
      {/* <div className="row">
        <p className="Label">Creation Date</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={creationDate}
          onChange={(e) => creationDateInput(e)}
        />
  </div>*/}
      <div className="row">
        <p className="Label">Due Date</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={dueDate}
          onChange={(e) => dueDateInput(e)}
        />
      </div>
      {/*<div className="row">
        <p className="Label">Closing Date</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={closingDate}
          onChange={(e) => closingDateInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Hour spent</p>
        <input
          className="inputField"
          autoFocus
          value={hourSpent}
          onChange={(e) => hourSpentInput(e)}
        />
</div>
      <div className="row">
        <p className="Label">Cost spent</p>
        <input
          className="inputField"
          autoFocus
          value={costSpent}
          onChange={(e) => costSpentInput(e)}
        />
      </div>*/}
      <div className="row">
        <p className="Label" style={{ flex: "1" }}>
          Status
        </p>

        <RadioGroup
          aria-label="gender"
          name="gender1"
          value={status.toUpperCase()}
          onChange={statusInput}
          style={{ flex: "2.3", flexDirection: "row" }}
        >
          <FormControlLabel
            value="OPEN"
            style={{ paddingBottom: "12px" }}
            disabled
            control={<Radio />}
            label="Open"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="CLOSED"
            disabled
            control={<Radio />}
            label="Closed"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="PAUSED"
            disabled
            control={<Radio />}
            label="Paused"
          />
        </RadioGroup>
      </div>
      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default EditProject;
