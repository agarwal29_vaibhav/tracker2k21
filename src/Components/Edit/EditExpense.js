import React, { useState } from "react";
import "./Edit.css";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import CloseIcon from "@material-ui/icons/Close";
import { HourglassEmpty } from "@material-ui/icons";
import axios from "axios";
import { Select, MenuItem } from "@material-ui/core";
import Notiflix from "notiflix";
import StarRateIcon from "@material-ui/icons/StarRate";

function EditExpense(props) {
  const { row } = props;
  const [Name, setName] = useState(row.name);
  const [Date, setDate] = useState(row.date);
  const [ExpenseCat, setExpenseCat] = useState(row.expenseCat);
  const [description, setdescription] = useState(row.description);
  const [amount, setamount] = useState(row.amount);
  const [attach, setattach] = useState(row.attach);
  const [voucher, setvoucher] = useState(row.voucher);
  const [firmName, setfirmName] = useState(row.firmname);
  const [coments, setcoments] = useState(row.comments);
  const [status, setstatus] = useState(row.status);

  const submitHandler = () => {
    const form = new FormData();

    form.append("id", row.id);
    form.append("file", attach);
    form.append("name", Name);
    form.append("description", description);
    form.append("expense_category", ExpenseCat);
    form.append("date", Date);
    form.append("amount", amount);
    form.append("voucher_number", voucher);
    form.append("firm", firmName);
    form.append("comments", coments);
    form.append("status", status);

    fetch("http://139.59.73.146:9090/v1/api/update/expense", {
      method: "POST",
      body: form,
    }).then((data) => {
      data
        .json()
        .then((info) => ({
          data: info,
          status: data.status,
        }))
        .then((res) => {
          if (res.data.status == "UPDATED" && firmName != "NA") {
            Notiflix.Notify.success("Expense Management updated sucessfully");
            window.location.reload();
          } else {
            Notiflix.Notify.failure("Please fill all the mandatory fileds.");
          }
        });
    });
  };
  // alert(firmName, "aa");

  const NameInput = (e) => {
    setName(e.target.value);
  };
  const dateInput = (e) => {
    setDate(e.target.value);
  };
  const ExpenseInput = (e) => {
    setExpenseCat(e.target.value);
  };
  const descriptionInput = (e) => {
    setdescription(e.target.value);
  };
  const amountInput = (e) => {
    setamount(e.target.value);
  };

  const attachInput = (e) => {
    e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onloadend = () => {
      setattach(file);
    };
    reader.readAsDataURL(file);
  };

  const VoucherInput = (e) => {
    setvoucher(e.target.value);
  };
  const FirmNameInput = (e) => {
    setfirmName(e.target.value);
  };
  const CommentInput = (e) => {
    setcoments(e.target.value);
  };
  const statusInput = (e) => {
    setstatus(e.target.value);
  };

  const resetHandler = () => {
    setName("");
    setDate("");
    setExpenseCat("");
    setdescription("");
    setamount("");
    setattach("");
    setvoucher("");
    setvoucher("");
    setcoments("");
  };
  const cancelHandler = () => {
    props.modalClosed();
  };

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  console.log(loginObject, "!!!!!!");
  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">Edit Expense Management</p>
        <p className="close" onClick={cancelHandler}>
          <CloseIcon />
        </p>
      </div>
      {/*<div className="row">
        <p className="Label">Name</p>
        <input
          className="inputField"
          autoFocus
          value={Name}
          onChange={(e) => NameInput(e)}
        />
  </div>*/}
      <div className="row">
        <p className="Label">Date</p>
        <input
          className="inputField"
          autoFocus
          value={Date}
          type="date"
          onChange={(e) => dateInput(e)}
        />
      </div>

      {/*<div className="row">
        <p className="Label">Voucher No.</p>
        <input
          className="inputField"
          autoFocus
          value={voucher}
          onChange={(e) => VoucherInput(e)}
        />
</div>*/}

      <div className="row">
        <p className="Label">Expense Category</p>
        <Select
          className="inputField"
          autoFocus
          value={ExpenseCat}
          onChange={(e) => ExpenseInput(e)}
        >
          {props.Expenselist.map((manager) => {
            return <MenuItem value={manager}>{manager}</MenuItem>;
          })}
        </Select>
      </div>

      <div className="row">
        <p className="Label">Description</p>
        <input
          className="inputField"
          autoFocus
          value={description}
          onChange={(e) => descriptionInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Amount</p>
        <input
          className="inputField"
          value={amount}
          onChange={(e) => amountInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">
          Attach PDF/ JPEG{" "}
          <StarRateIcon
            style={{
              height: "10px",
              width: "10px",
              color: "red",
              marginBottom: "10px",
            }}
          />
          <a
            onClick={() => {
              axios
                .get("http://139.59.73.146:9090/v1/api/downloadFile/" + attach)
                .then((response) => {
                  window.open(response.config.url, "_blank");
                  //  Notiflix.Notify.success("DOWNLOADED SUCCESSFULLY");
                });
            }}
            style={{
              color: "blue",
              cursor: "pointer",
              display: attach === "" ? "none" : "",
            }}
          >
            (View)
          </a>
        </p>
        <input
          className="inputField"
          onChange={(e) => attachInput(e)}
          type="file"
        ></input>
      </div>
      <div className="row">
        <p className="Label">
          Firm Name{" "}
          <StarRateIcon
            style={{
              height: "10px",
              width: "10px",
              color: "red",
              marginBottom: "10px",
            }}
          />
        </p>
        <Select
          className="inputField"
          autoFocus
          value={firmName}
          onChange={(e) => FirmNameInput(e)}
        >
          {props.FirmList.map((manager) => {
            return <MenuItem value={manager}>{manager}</MenuItem>;
          })}
        </Select>
      </div>
      <div className="row">
        <p className="Label">Comments</p>
        <input
          className="inputField"
          autoFocus
          value={coments}
          onChange={(e) => CommentInput(e)}
        />
      </div>

      {loginObject.userType == "ADMIN" || loginObject.userType == "SUBADMIN" ? (
        <div className="row">
          <p className="Label" style={{ flex: "1" }}>
            Status
          </p>

          <RadioGroup
            aria-label="gender"
            name="gender1"
            value={status.toUpperCase()}
            onChange={statusInput}
            style={{ flex: "2.3", flexDirection: "row" }}
          >
            <FormControlLabel
              value="REVIEWED"
              style={{ paddingBottom: "12px" }}
              control={<Radio />}
              label="Reviewed"
            />
            <FormControlLabel
              style={{ paddingBottom: "12px" }}
              value="PENDING REVIEW"
              control={<Radio />}
              label="Pending Review"
            />
            <FormControlLabel
              style={{ paddingBottom: "12px" }}
              value="OPEN"
              control={<Radio />}
              label="Open"
            />
          </RadioGroup>
        </div>
      ) : null}

      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default EditExpense;

//edit project
// manager should be dropdown

//project status
// time spent replace by cost spent
//non-editable  project, client, manager, due date project, timespent, cost pent
// time spent replace with cost spent
// user should be dropdown
