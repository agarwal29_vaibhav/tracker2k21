import React, { useState } from "react";
import "./Edit.css";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import Notiflix from "notiflix";

function EditProjectStatus(props) {
  console.log(props.row, "@@@");
  const { row } = props;
  const [projectName, setprojectName] = useState(row.project);
  const [client, setclient] = useState(row.client);
  const [manager, setmanager] = useState(row.manager);
  const [managerRemarks, setManagerRemarks] = useState(row.managerremark);
  const [user, setuser] = useState(row.user);
  const [userRemarks, setUserRemarks] = useState(row.userremark);
  const [dueDateUser, setdueDateUser] = useState(row.dueDateuser);
  const [dueDate, setdueDate] = useState(row.dueDateproject);
  const [hourSpent, sethourSpent] = useState(row.hourspent);
  const [costSpent, setcostSpent] = useState(row.costspent);
  const [status, setstatus] = useState(row.status);

  const projectInput = (e) => {
    setprojectName(e.target.value);
  };
  const clientInput = (e) => {
    setclient(e.target.value);
  };
  const managerAsignedInput = (e) => {
    setmanager(e.target.value);
  };
  const managerRemarksAsignedInput = (e) => {
    setManagerRemarks(e.target.value);
  };
  const userInput = (e) => {
    setuser(e.target.value);
  };
  const userRemarksInput = (e) => {
    setUserRemarks(e.target.value);
  };
  const dueDateUserInput = (e) => {
    setdueDateUser(e.target.value);
  };
  const dueDateInput = (e) => {
    setdueDate(e.target.value);
  };
  const hourSpentInput = (e) => {
    sethourSpent(e.target.value);
  };
  const costSpentInput = (e) => {
    setcostSpent(e.target.value);
  };
  const statusInput = (e) => {
    setstatus(e.target.value);
  };

  const submitHandler = () => {
    axios
      .post("http://139.59.73.146:9090/v1/api/update/projectstatus", {
        id: row.id,
        status: status,
      })
      .then((res) => {
        if (res.data.status == "UPDATED") {
          Notiflix.Notify.success("Project updated sucessfully");
          window.location.reload();
        } else {
          Notiflix.Notify.warning(res.data.status);
        }
      });
  };

  const resetHandler = () => {
    setprojectName("");
    setclient("");
    setmanager("");
    setManagerRemarks("");
    setuser("");
    setUserRemarks("");
    setdueDateUser("");
    setdueDate("");
    sethourSpent("");
    setcostSpent("");
    setstatus("");
  };

  const cancelHandler = () => {
    props.modalClosed();
  };

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">Edit Project Status</p>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <p className="Label">Project</p>
        <input
          className="inputField"
          autoFocus
          value={projectName}
          onChange={(e) => projectInput(e)}
          disabled={true}
        />
      </div>
      <div className="row">
        <p className="Label">Client</p>
        <input
          className="inputField"
          autoFocus
          value={client}
          onChange={(e) => clientInput(e)}
          disabled={true}
        />
      </div>
      <div className="row">
        <p className="Label">Manager Assigned</p>
        <input
          className="inputField"
          autoFocus
          value={manager}
          onChange={(e) => managerAsignedInput(e)}
          disabled={true}
        />
      </div>

      {/* <div className="row">
        <p className="Label">Hour spent</p>
        <input
          className="inputField"
          autoFocus
          value={hourSpent}
          onChange={(e) => hourSpentInput(e)}
          disabled={true}
        />
      </div>
      <div className="row">
        <p className="Label">Cost spent</p>
        <input
          className="inputField"
          autoFocus
          value={costSpent}
          onChange={(e) => costSpentInput(e)}
          disabled={true}
        />
      </div> */}

      {/*<div className="row">
        <p className="Label">User</p>
        <input
          className="inputField"
          autoFocus
          value={user}
          onChange={(e) => userInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">User Remarks</p>
        <input
          className="inputField"
          autoFocus
          value={userRemarks}
          onChange={(e) => userRemarksInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Manager Remarks</p>
        <input
          className="inputField"
          autoFocus
          value={managerRemarks}
          onChange={(e) => managerRemarksAsignedInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Due Date User Task</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={dueDateUser}
          onChange={(e) => dueDateUserInput(e)}
        />
  </div>*/}
      {/*  <div className="row">
        <p className="Label">Due Date</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={dueDate}
          onChange={(e) => dueDateInput(e)}
        />
    </div>*/}
      <div className="row">
        <p className="Label" style={{ flex: "1" }}>
          Status
        </p>

        <RadioGroup
          aria-label="gender"
          name="gender1"
          value={status}
          onChange={statusInput}
          style={{ flex: "2.3", flexDirection: "row" }}
        >
          <FormControlLabel
            value="OPEN"
            style={{ paddingBottom: "12px" }}
            control={<Radio />}
            label="OPEN"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="CLOSED"
            control={<Radio />}
            label="CLOSED"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="PAUSED"
            control={<Radio />}
            label="PAUSED"
          />
        </RadioGroup>
      </div>
      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default EditProjectStatus;
