import React, { useState } from "react";
import "./Edit.css";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import Notiflix from "notiflix";
import moment from "moment";
import { Select, MenuItem } from "@material-ui/core";

function EditSubProject(props) {
  const { row } = props;
  const [User, setUser] = useState(row.managerRemarks);
  const [managerRemarks, setManagerRemarks] = useState(row.user);
  const [UserRemarks, setUserRemarks] = useState(row.userRemarks);
  const [dueDate, setdueDate] = useState(row.userTaskDueDate);

  const projectInput = (e) => {
    setManagerRemarks(e.target.value);
  };
  const clientInput = (e) => {
    setUser(e.target.value);
  };
  const managerAsignedInput = (e) => {
    setUserRemarks(e.target.value);
  };
  const dueDateInput = (e) => {
    setdueDate(e.target.value);
  };

  const submitHandler = () => {
    var DUEDATE = moment(dueDate).format("YYYY-MM-DD");
    axios
      .post("http://139.59.73.146:9090/v1/api/update/sub/project", {
        sub_project_id: row.id,
        user: User,
        user_remarks: UserRemarks,
        manager_remarks: managerRemarks,
        user_task_due_date: DUEDATE,
      })
      .then((response) => {
        if (response.data.status === "UPDATED") {
          Notiflix.Notify.success("Project updated sucessfully");
          window.location.reload();
        } else {
          Notiflix.Notify.warning(response.data.status);
        }
      });
  };

  const resetHandler = () => {
    setManagerRemarks("");
    setUser("");
    setUserRemarks("");
    setdueDate("");
  };

  const cancelHandler = () => {
    props.modalClosed();
  };

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">Edit Sub-Project</p>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <p className="Label">Manager Remarks</p>
        <input
          className="inputField"
          autoFocus
          value={managerRemarks}
          onChange={(e) => projectInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">User</p>
        <Select
          className="inputField"
          autoFocus
          value={User}
          onChange={(e) => clientInput(e)}
        >
          {props.users && props.users.map((manager) => {
            return <MenuItem value={manager.toLowerCase()}>{manager}</MenuItem>;
          })}
        </Select>
      </div>
      <div className="row">
        <p className="Label">User Remarks</p>
        <input
          className="inputField"
          autoFocus
          value={UserRemarks}
          onChange={(e) => managerAsignedInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Due Date User Task</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={dueDate}
          onChange={(e) => dueDateInput(e)}
        />
      </div>
      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default EditSubProject;
