import React, { useState, useEffect } from "react";
import "./LeftPannel.css";
import logo from "../../Assets/logo.png";
import logout from "../../Assets/logout.png";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect,
} from "react-router-dom";
import Login from "../Login/Login";
import Table from "../../UI/Table/Table";
import TeamTable from "../../UI/TeamTable/TeamTable";
import ProjectStatus from "../../UI/ProjectStatus/ProjectStatus";
import TimesheetTable from "../Timesheet/TimesheetTable";
import ExpenseManagment from "../../UI/ExpenseManagment/ExpenseManagment";

function LeftPannel() {
  const [state, setstate] = React.useState(window.location.pathname);
  const [loginData, setLoginData] = useState("");

  useEffect(() => {
    var loginData = JSON.parse(localStorage.getItem("LoginDetails"));
    if (loginData !== null) {
      setLoginData(loginData);
    }
  }, [state]);

  useEffect(() => {
    setstate(window.location.pathname);
  }, [window.location.pathname]);

  if (loginData.userType === "ADMIN") {
    return (
      <div className="inColumn">
        <Router>
          <div className="leftPannel">
            <div className="options">
              <Link to={`/projects`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/projects` ? "selectedLabel" : "pannelLabel"
                  }
                  onClick={() => setstate(`/projects`)}
                >
                  Project
                </p>
              </Link>
              <Link to={`/teams`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/teams` ? "selectedLabel" : "pannelLabel"
                  }
                  onClick={() => setstate(`/teams`)}
                >
                  Team
                </p>
              </Link>
              <Link to={`/productStatus`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/productStatus` ? "selectedLabel" : "pannelLabel"
                  }
                  onClick={() => setstate(`/productStatus`)}
                >
                  Project Status
                </p>
              </Link>
              <Link to={`/timesheet`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/timesheet` ? "selectedLabel" : "pannelLabel"
                  }
                  onClick={() => setstate(`/timesheet`)}
                >
                  Timesheet
                </p>
              </Link>
              <Link to={`/expenseManagment`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/expenseManagment`
                      ? "selectedLabel"
                      : "pannelLabel"
                  }
                  onClick={() => setstate(`/expenseManagment`)}
                >
                  Expense Management
                </p>
              </Link>
            </div>
          </div>

          <div className="rightDiv">
            <Switch>
              {/*<Route exact path={`/`} component={Login}></Route>*/}
              <Route exact path={`/projects`} component={Table} />
              <Route exact path={`/teams`} component={TeamTable} />
              <Route exact path={`/productStatus`} component={ProjectStatus} />
              <Route exact path={`/timesheet`} component={TimesheetTable} />
              <Route
                exact
                path={`/expenseManagment`}
                component={ExpenseManagment}
              />
            </Switch>
          </div>
        </Router>
      </div>
    );
  } else {
    return (
      <div className="inRow">
        <Router>
          <div className="leftPannel">
            <div className="options">
              <Link to={`/productStatus`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/productStatus` ? "selectedLabel" : "pannelLabel"
                  }
                  onClick={() => setstate(`/productStatus`)}
                >
                  Project Status
                </p>
              </Link>
              <Link to={`/timesheet`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/timesheet` ? "selectedLabel" : "pannelLabel"
                  }
                  onClick={() => setstate(`/timesheet`)}
                >
                  Timesheet
                </p>
              </Link>
              <Link to={`/expenseManagment`} style={{ textDecoration: "none" }}>
                <p
                  className={
                    state == `/expenseManagment`
                      ? "selectedLabel"
                      : "pannelLabel"
                  }
                  onClick={() => setstate(`/expenseManagment`)}
                >
                  Expense Management
                </p>
              </Link>
            </div>
          </div>

          <div className="rightDiv">
            <Switch>
              {/*<Route exact path={`/`} component={Login}></Route>*/}
              <Route exact path={`/projects`} component={Table} />
              <Route exact path={`/teams`} component={TeamTable} />
              <Route exact path={`/productStatus`} component={ProjectStatus} />
              <Route exact path={`/timesheet`} component={TimesheetTable} />
              <Route
                exact
                path={`/expenseManagment`}
                component={ExpenseManagment}
              />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default LeftPannel;
