import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import Notiflix from "notiflix";
import "./delete.css";

function DeleteTeam(props) {
  const deleteHandler = () => {
    axios
      .delete("http://139.59.73.146:9090/v1/api/delete/team", {
        data: {
          id: +props.row,
        },
      })
      .then((res) => {
        if (res.data.status == "DELETED") {
          Notiflix.Notify.success("Team deleted sucessfully");
        } else {
          Notiflix.Notify.warning(res.data.status);
        }
        window.location.reload();
        props.modalClosed();
      });
  };

  const cancelHandler = () => {
    props.modalClosed();
  };

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <h1>Are you sure?</h1>
      </div>
      <div className="row subDeleteHeading">
        Do you really want to delete these records? This process cannot be
        undone.
      </div>
      <div className="btnDiv">
        <button className="cancelBtn" onClick={cancelHandler}>
          Cancel
        </button>
        <button className="deleteBtn" onClick={deleteHandler}>
          Delete
        </button>
      </div>
    </div>
  );
}

export default DeleteTeam;
