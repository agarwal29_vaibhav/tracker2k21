import React, { useState } from "react";
import "./Create.css";
import CloseIcon from "@material-ui/icons/Close";
import Notiflix from "notiflix";
import { Select, MenuItem } from "@material-ui/core";

function CreateExpense(props) {
  const [Name, setName] = useState("");
  const [Date, setDate] = useState("");
  const [ExpenseCat, setExpenseCat] = useState("");
  const [description, setdescription] = useState("");
  const [amount, setamount] = useState("");
  const [attach, setattach] = useState('');
  const [voucher, setvoucher] = useState("");

  const NameInput = (e) => {
    setName(e.target.value);
  };
  const dateInput = (e) => {
    setDate(e.target.value);
  };
  const ExpenseInput = (e) => {
    setExpenseCat(e.target.value);
  };
  const descriptionInput = (e) => {
    setdescription(e.target.value);
  };
  const amountInput = (e) => {
    setamount(e.target.value);
  };

  const attachInput = (e) => {
    e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onloadend = () => {
      setattach(file);
    };
    reader.readAsDataURL(file);
  };

  const voucherInput = (e) => {
    setvoucher(e.target.value);
  };

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  let use_name = loginObject.name;
  const submitHandler = () => {
    // api to be integrtaed
    // if (attach.length > 0) {

    const form = new FormData();

    form.append("file", attach);
    form.append("name", Name);
    form.append("description", description);
    form.append("expense_category", ExpenseCat);
    form.append("date", Date);
    form.append("amount", amount);
    form.append("voucher_number", voucher);
    form.append("user_name", use_name);

    fetch("http://139.59.73.146:9090/v1/api/create/expense", {
      method: "POST",
      body: form,
    }).then((data) => {
      data
        .json()
        .then((info) => ({
          data: info,
        
          status: data.status,
        }))
        .then((res) => {
          if (res.status == 200) {
            
            Notiflix.Notify.success("Expense Created Sucessfully");
            window.location.reload();
          }
        });
    });

  // } else 
  // {
  // Notiflix.Notify.failure("Kindly Upload File.");
  // }
  };
  const resetHandler = () => {
    setName("");
    setDate("");
    setExpenseCat("");
    setdescription("");
    setamount("");
    setattach("");
    setvoucher("");
  };
  const cancelHandler = () => {
    props.setopenModal(false);
  };

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">
          Create Expense Management
        </p>
        <p className="close" onClick={cancelHandler}>
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <p className="Label">Name</p>
        <input
          className="inputField"
          autoFocus
          value={Name}
          onChange={(e) => NameInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Date</p>
        <input
          className="inputField"
          autoFocus
          value={Date}
          type="date"
          onChange={(e) => dateInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Expense Category</p>
        <Select
          className="inputField"
          autoFocus
          value={ExpenseCat}
          onChange={(e) => ExpenseInput(e)}
        >
          {props.Expenselist.map((manager) => {
            return <MenuItem value={manager}>{manager}</MenuItem>;
          })}
        </Select>
      </div>

      <div className="row">
        <p className="Label">Description</p>
        <input
          className="inputField"
          autoFocus
          value={description}
          onChange={(e) => descriptionInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Voucher No.</p>
        <input
          className="inputField"
          autoFocus
          value={voucher}
          type="number"
          onChange={(e) => voucherInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Amount</p>
        <input
          className="inputField"
          autoFocus
          type="number"
          value={amount}
          onChange={(e) => amountInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Attach PDF/ JPEG</p>
        <input
          className="inputField"
          onChange={(e) => attachInput(e)}
          type="file"
        />
      </div>

      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default CreateExpense;
