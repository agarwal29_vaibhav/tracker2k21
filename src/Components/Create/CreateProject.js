import React, { useEffect, useState } from "react";
import "./Create.css";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Select, MenuItem } from "@material-ui/core";
import SelectWithInput from "../../UI/SelectWithInput";
import FormLabel from "@material-ui/core/FormLabel";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import moment from "moment";
import Notiflix from "notiflix";

function CreateProject(props) {
  const [projectName, setprojectName] = useState("");
  const [client, setClient] = useState("");
  const [managerAsigned, setmanagerAsigned] = useState("");
  const [dueDate, setdueDate] = useState("");
  const [status, setstatus] = useState("");
  const [Billable, setBillable] = useState("");
  const [clientList, setClientList] = useState([]);
  const projectInput = (e) => {
    setprojectName(e.target.value);
  };
  const clientInput = (e) => {
    setClient(e.target.value);
  };
  const managerAsignedInput = (e) => {
    setmanagerAsigned(e.target.value);
  };
  const dueDateInput = (e) => {
    setdueDate(e.target.value);
  };
  const statusInput = (e) => {
    setstatus(e.target.value);
  };

  const BillableInput = (event) => {
    setBillable(event.target.value);
  };

  const submitHandler = () => {
    var DUEDATE = moment(dueDate).format("YYYY-MM-DD");
    axios
      .post("http://139.59.73.146:9090/v1/api/create/project", {
        name: projectName,
        client: client,
        billable: Billable,
        manager_assigned: managerAsigned,
        status: "OPEN",
        due_date: DUEDATE,
      })
      .then((response) => {
        if (response.data.status === "ADDED") {
          Notiflix.Notify.success("Project Created Sucessfully");
          window.location.reload();
        } else {
          Notiflix.Notify.warning(response.data.status);
        }
      });
  };
  const resetHandler = () => {
    setprojectName("");
    setClient("");
    setmanagerAsigned("");
    setdueDate("");
    // sethourSpent("");
    // setcostSpent("");
    setstatus("");
    setBillable("");
  };

  const cancelHandler = () => {
    props.setopenModal(false);
  };

  useEffect(() => {
    // client dropdown
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/client")
      .then((res) => {
        setClientList(res.data.data[0]);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  console.log(clientList);

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">Create Project</p>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <p className="Label">Project</p>
        <input
          className="inputField"
          autoFocus
          value={projectName}
          onChange={(e) => projectInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Client</p>
        <div className="inputField">
          <SelectWithInput
            dropdownOptions={clientList}
            showConstValue={true}
            value={client}
            setValue={setClient}
          />
        </div>
      </div>
      <div className="row">
        <p className="Label" style={{ flex: "1" }}>
          Billable
        </p>
        <RadioGroup
          aria-label="gender"
          name="gender1"
          value={Billable}
          onChange={BillableInput}
          style={{ flex: "2.3", flexDirection: "row" }}
        >
          <FormControlLabel
            value="True"
            style={{ paddingBottom: "12px" }}
            control={<Radio />}
            label="True"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="False"
            control={<Radio />}
            label="False"
          />
        </RadioGroup>
      </div>
      <div className="row">
        <p className="Label">Manager Assigned</p>
        <Select
          className="inputField"
          autoFocus
          value={managerAsigned}
          onChange={(e) => managerAsignedInput(e)}
        >
          {props.managerList.map((manager) => {
            return <MenuItem value={manager}>{manager}</MenuItem>;
          })}
        </Select>
      </div>
      <div className="row">
        <p className="Label">Due Date</p>
        <input
          className="inputField"
          autoFocus
          type="date"
          value={dueDate}
          onChange={(e) => dueDateInput(e)}
        />
      </div>

      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default CreateProject;
