import React, { useState } from "react";
import "./Create.css";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";
import Notiflix from "notiflix";

function CreateTeam(props) {
  const [Name, setName] = useState("");
  const [EmailId, setEmailId] = useState("");
  const [LoginId, setLoginId] = useState("");
  const [password, setpassword] = useState("");
  const [hourRate, sethourRate] = useState("");
  const [status, setstatus] = useState("");

  const NameInput = (e) => {
    setName(e.target.value);
  };
  const emailIDInput = (e) => {
    setEmailId(e.target.value);
  };
  const LoginIdInput = (e) => {
    setLoginId(e.target.value);
  };
  const passwordInput = (e) => {
    setpassword(e.target.value);
  };
  const hourRateInput = (e) => {
    sethourRate(e.target.value);
  };

  const statusInput = (e) => {
    setstatus(e.target.value);
  };

  const submitHandler = () => {
    axios
      .post("http://139.59.73.146:9090/v1/api/create/team", {
        name: Name,
        email_id: EmailId,
        user_id: LoginId,
        user_password: password,
        status: status,
        hourly_rate: hourRate,
      })
      .then((res) => {
        if (res.data.status == "ADDED") {
          Notiflix.Notify.success("Team created sucessfully");
          window.location.reload();
        } else {
          Notiflix.Notify.warning(res.data.status);
        }
      });
  };
  const resetHandler = () => {
    setName("");
    setEmailId("");
    setLoginId("");
    setpassword("");
    sethourRate("");
    setstatus("");
  };
  const cancelHandler = () => {
    props.setopenModal(false);
  };

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">Create Team</p>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <div className="row">
        <p className="Label">Name</p>
        <input
          className="inputField"
          autoFocus
          value={Name}
          onChange={(e) => NameInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Email Id</p>
        <input
          className="inputField"
          autoFocus
          value={EmailId}
          onChange={(e) => emailIDInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Login Id</p>
        <input
          className="inputField"
          autoFocus
          value={LoginId}
          onChange={(e) => LoginIdInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label">Password</p>
        <input
          className="inputField"
          autoFocus
          value={password}
          onChange={(e) => passwordInput(e)}
        />
      </div>
      <div className="row">
        <p className="Label">Hour Rate</p>
        <input
          className="inputField"
          autoFocus
          type="number"
          value={hourRate}
          onChange={(e) => hourRateInput(e)}
        />
      </div>

      <div className="row">
        <p className="Label" style={{ flex: "1" }}>
          Status
        </p>

        <RadioGroup
          aria-label="gender"
          name="gender1"
          value={status}
          onChange={statusInput}
          style={{ flex: "2.3", flexDirection: "row" }}
        >
          <FormControlLabel
            value="ACTIVE"
            style={{ paddingBottom: "12px" }}
            control={<Radio />}
            label="ACTIVE"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="INACTIVE"
            control={<Radio />}
            label="INACTIVE"
          />
          <FormControlLabel
            style={{ paddingBottom: "12px" }}
            value="INVALID"
            control={<Radio />}
            label="INVALID"
          />
        </RadioGroup>
      </div>

      <div className="row" style={{ justifyContent: "center" }}>
        <button className="submitBtn" onClick={submitHandler}>
          Submit
        </button>
        <button className="resetBtn" onClick={resetHandler}>
          Reset
        </button>
      </div>
    </div>
  );
}

export default CreateTeam;
