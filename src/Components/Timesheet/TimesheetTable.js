import React, { useEffect, useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {
  Button,
  TextField,
  Select,
  NativeSelect,
  MenuItem,
  ClickAwayListener,
} from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import moment from "moment";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import TablePagination from "@material-ui/core/TablePagination";
import "./Timesheet.css";
import Header from "../../UI/Header/Header";
import calendarIcon from "../../Assets/calendar.svg";
import axios from "axios";
import Notiflix from "notiflix";
import CircularProgress from "@material-ui/core/CircularProgress";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SelectDropdown from "../../UI/Dropdown/SelectDropdown";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#FFF1E1",
    color: "#4E4E4E",
    fontWeight: "600",
  },
  body: {
    fontSize: 14,
    borderBottom: 0,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    position: "relative",
    background: "#F8F8F8 0% 0% no-repeat padding-box",
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    width: "100%",
    borderSpacing: "5px",
    borderCollapse: "separate",
  },
  tableContainer: {
    boxShadow: "none",
    minHeight: "68vh",
    maxHeight: "68vh",
    overflowY: "auto",
    width: "100%",
    overflowX: "auto",
    "&::-webkit-scrollbar": {
      width: "10px",
      height: "15px",
      backgroundColor: "white",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#DADADA",
      borderRadius: "10px",
    },
  },
  pagination: {
    bottom: "4rem",
    right: "0",
    width: "40%",
    zIndex: "1",
    position: "absolute",
  },
  textField: {
    width: 150,
    opacity: "0",
    cursor: "pointer",
    "& label": {
      color: "#000000",
    },
    "& input": {
      fontSize: "14px",
      color: "#808080",
      cursor: "pointer",
    },
  },
  tableRow: {
    "&:first-child": {
      borderTopLeftRadius: "4vh",
    },
    "&:last-child": {
      borderTopRightRadius: "4vh",
    },
  },
});

export default function TimesheetTable() {
  const classes = useStyles();
  var date = new Date();

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  if (!loginObj || loginObj === null) {
    window.location.href = "/";
  }
  var formatted = moment(date).format("YYYY-MM-DD");
  const count = 7;
  const [data, setData] = useState([
    {
      client: null,
      date: null,
      description: null,
      hoursSpent: null,
      id: null,
      project: null,
      user_id: null,
    },
  ]);
  // const [projectDropdown, setProjectDropdown] = useState([]);
  const [clientDropdown, setClientDropdown] = useState([]);
  const [dateArray, setDateArray] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [selectedProject, setSelectedProject] = useState([]);
  const [selectedClient, setSelectedClient] = useState([]);
  const [hoursSpent, setHoursSpent] = useState([]);
  const [description, setDescription] = useState([]);
  const [showLoader, setShowLoader] = useState(true);
  const [showClientDrop, setShowClientDrop] = useState(null);
  const [showProjectDrop, setShowProjectDrop] = useState(null);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  useEffect(() => {
    let arr = [];
    for (let i = 0; i < count; i++) {
      var last = new Date(date.getTime() + i * 24 * 60 * 60 * 1000);
      var day = last.getDate();
      var month = last.getMonth() + 1;
      var year = last.getFullYear();
      var formatted1 = moment(new Date(`${month}-${day}-${year}`)).format(
        "DD/MM/YYYY"
      );
      arr.push(formatted1);
    }
    setDateArray(arr);
    calenderAPI(arr);
  }, []);

  const calenderAPI = (arr) => {
    let from = moment(moment(arr[0], "DD/MM/YYYY").toDate()).format(
      "YYYY-MM-DD"
    );
    let to = moment(moment(arr[6], "DD/MM/YYYY").toDate()).format("YYYY-MM-DD");
    let obj = {
      user_id: loginObject.userId,
      from: from,
      to: to,
    };
    axios
      .post("http://139.59.73.146:9090/v1/api/get/time", obj)
      .then((res) => {
        res.data.data &&
          res.data.data.forEach((elem, index) => {
            elem.timesheet_data.forEach((date1) => {
              setHoursSpent((prev) => {
                let a = [...prev];
                a.push([null, null, null, null, null, null, null]);
                arr &&
                  arr.map((elem1, index2) => {
                    if (
                      moment(moment(elem1, "DD/MM/YYYY").toDate()).format(
                        "YYYY-MM-DD"
                      ) === date1.date
                    ) {
                      a[index][index2] = date1.hours_spent;
                    }
                  });
                return a;
              });
              setDescription((prev) => {
                let a = [...prev];
                a.push([null, null, null, null, null, null, null]);
                arr &&
                  arr.map((elem1, index2) => {
                    if (
                      moment(moment(elem1, "DD/MM/YYYY").toDate()).format(
                        "YYYY-MM-DD"
                      ) === date1.date
                    ) {
                      a[index][index2] = date1.description;
                    }
                  });
                return a;
              });
            });

            if (selectedProject.length > 0) {
              setSelectedProject((prev) => {
                let a = [...prev];
                a[index] = elem.project;
                return a;
              });
            } else {
              setSelectedProject((prev) => [...prev, elem.project]);
            }
            if (selectedClient.length > 0) {
              setSelectedClient((prev) => {
                let a = [...prev];
                a[index] = elem.client;
                return a;
              });
              axios
                .post(
                  "http://139.59.73.146:9090/v1/api/get/timesheet/project",
                  {
                    user_name: loginObject.name,
                    client_name: elem.client,
                  }
                )
                .then((res) => {
                  elem.projectDropdown = res.data.data;
                })
                .catch((err) => console.log(err));
            } else {
              setSelectedClient((prev) => [...prev, elem.client]);
              axios
                .post(
                  "http://139.59.73.146:9090/v1/api/get/timesheet/project",
                  {
                    user_name: loginObject.name,
                    client_name: elem.client,
                  }
                )
                .then((res) => {
                  elem.projectDropdown = res.data.data;
                })
                .catch((err) => console.log(err));
            }
          });
        if (res.data.data.length > 0) {
          setData(res.data.data);
        } else {
          setData([
            {
              client: "Select Client",
              date: null,
              description: null,
              hoursSpent: null,
              id: null,
              project: "Select Project",
              user_id: null,
            },
          ]);
          setHoursSpent((prev) => [
            ...prev,
            [null, null, null, null, null, null, null],
          ]);
          setDescription((prev) => [
            ...prev,
            [null, null, null, null, null, null, null],
          ]);
        }
        setShowLoader(false);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    // axios
    //   .get("http://139.59.73.146:9090/v1/api/get/filters/project")
    //   .then((res) => {
    //     let newArr = res.data.data;
    //     setProjectDropdown((prev) => {
    //       let arr = [...prev];
    //       let a = newArr[0].filter((x, index) => {
    //         if (!arr.includes(x[0])) {
    //           arr.push(x[0]);
    //           return [x[0], x[1]];
    //         }
    //       });
    //       a.splice(0, 0, ["Select Project", 0]);
    //       return a;
    //     });
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/client")
      .then((res) => {
        let newArr = res.data.data;
        newArr[0].splice(0, 0, "Select Client");
        setClientDropdown(
          newArr[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleDateChange = (value) => {
    setShowLoader(true);
    setHoursSpent([]);
    setDescription([]);
    setSelectedProject([]);
    setSelectedClient([]);
    let myDate = moment(value, "YYYY-MM-DD").toDate();
    let arr = [];
    for (let i = 0; i < count; i++) {
      var last = new Date(myDate.getTime() + i * 24 * 60 * 60 * 1000);
      var day = last.getDate();
      var month = last.getMonth() + 1;
      var year = last.getFullYear();
      var formatted1 = moment(new Date(`${month}-${day}-${year}`)).format(
        "DD/MM/YYYY"
      );
      arr.push(formatted1);
    }
    setDateArray(arr);
    calenderAPI(arr);
  };

  const handleSubmit = (index, row) => {
    let projectId = row.projectDropdown.filter((elem) => {
      if (elem[0] == selectedProject[index]) {
        return elem[1];
      }
    });
    let condition = 0;
    let addObj = dateArray.map((date, index1) => {
      if (description[index][index1] || hoursSpent[index][index1]) {
        condition = 1;
      }
      return {
        project:
          selectedProject[index] === "Select Project" || !selectedProject[index]
            ? null
            : selectedProject[index],
        project_id: projectId[0] ? projectId[0][1] : null,
        user_id: loginObject.userId,
        client:
          selectedClient[index] === "Select Client" || !selectedClient[index]
            ? null
            : selectedClient[index],
        date: moment(moment(date, "DD/MM/YYYY").toDate()).format("YYYY-MM-DD"),
        description: description[index][index1]
          ? description[index][index1]
          : null,
        hours_spent: hoursSpent[index][index1]
          ? +hoursSpent[index][index1]
          : null,
      };
    });
    if (condition === 1) {
      axios
        .post("http://139.59.73.146:9090/v1/api/add/time", addObj)
        .then((res) => {
          if (res.data.status == "ADDED") {
            Notiflix.Notify.success("Time Added sucessfully");
          } else {
            Notiflix.Notify.warning(res.data.status);
          }
        })
        .catch((err) => console.log(err));
    } else {
      Notiflix.Notify.warning(
        "Add atleast one description or add atleast one hours column"
      );
    }
  };

  const addNewRow = () => {
    const obj = {
      client: "Select Client",
      date: null,
      description: null,
      hoursSpent: null,
      id: null,
      project: "Select Project",
      user_id: null,
      projectDropdown: null,
    };
    setData((prev) => [...prev, obj]);
    setHoursSpent((prev) => [
      ...prev,
      [null, null, null, null, null, null, null],
    ]);
    setDescription((prev) => [
      ...prev,
      [null, null, null, null, null, null, null],
    ]);
  };

  const deleteNewRow = () => {
    setData((prev) => {
      let a = [...prev];
      a.pop();
      return a;
    });
    setHoursSpent((prev) => {
      let a = [...prev];
      a.pop();
      return a;
    });
    setDescription((prev) => {
      let a = [...prev];
      a.pop();
      return a;
    });
  };
  const clientHandler = (val, index) => {
    axios
      .post("http://139.59.73.146:9090/v1/api/get/timesheet/project", {
        user_name: loginObject.name,
        client_name: val,
      })
      .then((res) => {
        let a = [...data];
        a[index].projectDropdown = res.data.data;
        setData(a);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div>
      <Header userName="user" title="Timesheet" />
      <div className="calenderDiv" style={{ marginBottom: "1rem" }}>
        <input
          id="date"
          type="date"
          onChange={(e) => handleDateChange(e.target.value)}
          className={classes.textField}
        />
        <span className="CalenderBtn">
          <img src={calendarIcon} style={{ cursor: "pointer" }} /> Calender
        </span>
      </div>
      {showLoader ? (
        <div className="loader">
          {" "}
          <CircularProgress />
        </div>
      ) : (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            className={classes.table}
            stickyHeader
            aria-label="sticky table"
          >
            <TableHead>
              <TableRow>
                <StyledTableCell className={classes.tableRow}>
                  Client
                </StyledTableCell>
                <StyledTableCell className={classes.tableRow}>
                  Project
                </StyledTableCell>
                {dateArray &&
                  dateArray.map((elem) => {
                    return (
                      <StyledTableCell className={classes.tableRow}>
                        {elem}
                      </StyledTableCell>
                    );
                  })}
                <StyledTableCell
                  style={{
                    backgroundColor: "white",
                    border: "none",
                    padding: "16px 36px",
                  }}
                ></StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, indexHead) => {
                  return (
                    <StyledTableRow key={row.name}>
                      <StyledTableCell
                        component="th"
                        scope="row"
                        style={{ width: "13vw", position: "relative" }}
                      >
                        <button
                          onClick={() =>
                            setShowClientDrop(indexHead + page * rowsPerPage)
                          }
                          className="buttonDropdown"
                        >
                          <span style={{ textAlign: "left" }}>
                            {selectedClient.length > 0 &&
                            selectedClient[indexHead + page * rowsPerPage]
                              ? selectedClient[indexHead + page * rowsPerPage]
                              : row.client}
                          </span>
                          <ArrowDropDownIcon
                            style={{
                              position: "absolute",
                              right: "0",
                              top: "30%",
                              cursor: "pointer",
                            }}
                          />
                        </button>
                        {showClientDrop === indexHead + page * rowsPerPage ? (
                          <ClickAwayListener
                            onClickAway={() => setShowClientDrop(null)}
                          >
                            <div>
                              <SelectDropdown
                                dropdownClass="dropdown2"
                                wholeList={clientDropdown}
                                onChange={(value) => {
                                  // alert(value);
                                  clientHandler(
                                    value,
                                    indexHead + page * rowsPerPage
                                  );
                                  setSelectedClient((prev) => {
                                    let a = [...prev];
                                    a[indexHead + page * rowsPerPage] = value;
                                    return a;
                                  });
                                }}
                                showInput={true}
                              />
                            </div>
                          </ClickAwayListener>
                        ) : null}
                      </StyledTableCell>
                      <StyledTableCell
                        component="th"
                        scope="row"
                        style={{ width: "13vw", position: "relative" }}
                      >
                        <button
                          onClick={() =>
                            setShowProjectDrop(indexHead + page * rowsPerPage)
                          }
                          className="buttonDropdown"
                        >
                          <span style={{ textAlign: "left" }}>
                            {selectedProject.length > 0 &&
                            selectedProject[indexHead + page * rowsPerPage]
                              ? selectedProject[indexHead + page * rowsPerPage]
                              : row.project}
                          </span>
                          <ArrowDropDownIcon
                            style={{
                              position: "absolute",
                              right: "0",
                              top: "30%",
                              cursor: "pointer",
                            }}
                          />
                        </button>
                        {showProjectDrop === indexHead + page * rowsPerPage ? (
                          <ClickAwayListener
                            onClickAway={() => setShowProjectDrop(null)}
                          >
                            <div>
                              <SelectDropdown
                                dropdownClass="dropdown2"
                                wholeList={row.projectDropdown}
                                onChange={(value) => {
                                  setSelectedProject((prev) => {
                                    let a = [...prev];
                                    a[indexHead + page * rowsPerPage] = value;
                                    return a;
                                  });
                                }}
                                project={true}
                                showInput={true}
                                noDataLabel="No Projects available for this client"
                              />
                            </div>
                          </ClickAwayListener>
                        ) : null}
                      </StyledTableCell>
                      {dateArray &&
                        dateArray.map((elem, index) => {
                          return (
                            <StyledTableCell style={{ padding: "4px 16px" }}>
                              {/* {elem.value} */}
                              <input
                                type="number"
                                className="upperInput"
                                placeholder="Enter Hours"
                                value={
                                  hoursSpent.length > 0 &&
                                  hoursSpent[indexHead + page * rowsPerPage]
                                    ? hoursSpent[
                                        indexHead + page * rowsPerPage
                                      ][index]
                                    : null
                                }
                                onChange={(e) =>
                                  setHoursSpent((prev) => {
                                    let newArray = [...prev];
                                    newArray[indexHead + page * rowsPerPage][
                                      index
                                    ] = e.target.value;
                                    return newArray;
                                  })
                                }
                              />
                              <input
                                type="text"
                                className="lowerInput"
                                placeholder="Enter Description"
                                value={
                                  description.length > 0 &&
                                  description[indexHead + page * rowsPerPage]
                                    ? description[
                                        indexHead + page * rowsPerPage
                                      ][index]
                                    : null
                                }
                                onChange={(e) =>
                                  setDescription((prev) => {
                                    let newArray = [...prev];
                                    newArray[indexHead + page * rowsPerPage][
                                      index
                                    ] = e.target.value;
                                    return newArray;
                                  })
                                }
                              />
                            </StyledTableCell>
                          );
                        })}
                      <StyledTableCell
                        style={{
                          backgroundColor: "white",
                          border: "none",
                          padding: "16px 0",
                        }}
                      >
                        <Button
                          style={{
                            position: "absolute",
                            background: "#FF9414",
                            textTransform: "capitalize",
                            fontWeight: "600",
                            letterSpacing: "1px",
                            top: "40%",
                            padding: "3px 8px",
                          }}
                          onClick={() =>
                            handleSubmit(indexHead + page * rowsPerPage, row)
                          }
                        >
                          Submit
                        </Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  );
                })}
              <AddCircleIcon
                style={{ color: "#FF9414", fontSize: "2.5rem" }}
                onClick={addNewRow}
              />
              <RemoveCircleIcon
                style={{ color: "#FF9414", fontSize: "2.5rem" }}
                onClick={deleteNewRow}
              />
            </TableBody>
          </Table>

          <TablePagination
            className={classes.pagination}
            rowsPerPageOptions={[5, 10, 15, 20]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </TableContainer>
      )}
    </div>
  );
}

// api url
// http://139.59.73.146:9090/v1/api/get/timesheet/project

//body
// {
//   "user_name":"Vikas Mittal",
//   "client_name":"Widex"
// }
