import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import "../Components/Delete/delete.css"

function Notification(props) {

  const cancelHandler = () => {
    props.modalClosed();
  };

  return (
    <div>
      <div className="row subDeleteHeading">
        <h1>{props.heading}</h1>
      </div>
      <div className="btnDiv">
        <button className="deleteBtn" onClick={cancelHandler}>
          OK
        </button>
      </div>
    </div>
  );
}

export default Notification;
