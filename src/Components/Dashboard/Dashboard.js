import React from "react";
import LeftPannel from "../LeftPannel/LeftPannel";
import Table from "../../UI/Table/Table";
import TimeSheet from "../Timesheet/TimesheetTable";
import TeamTable from "../../UI/TeamTable/TeamTable";
import "./Dashboard.css";
import ProjectStatus from "../../UI/ProjectStatus/ProjectStatus";
import ExpenseManagment from "../../UI/ExpenseManagment/ExpenseManagment";

function Dashboard() {
  return (
    <div className="inRow">
      <div className="leftDiv">
        <LeftPannel />
      </div>
      <div className="rightDiv">
        <ExpenseManagment />
      </div>
    </div>
  );
}

export default Dashboard;
