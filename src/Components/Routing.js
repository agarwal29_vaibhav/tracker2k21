import React from "react";
import LeftPannel from "./LeftPannel/LeftPannel";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Login from "./Login/Login";

function Routing() {
  return (
    <div>
      <Route
        path="/"
        render={(routeProps) => {
          return (
            JSON.parse(localStorage.getItem("LoginDetails")) == null && (
              <Redirect to="/" />
            )
          );
        }}
      />
      <Route exact path={`/`} component={Login}></Route>
      <Route exact path={`/projects`} component={LeftPannel}></Route>
      <Route exact path={`/timesheet`} component={LeftPannel}></Route>
      <Route exact path={`/teams`} component={LeftPannel}></Route>
      <Route exact path={`/productStatus`} component={LeftPannel}></Route>
      <Route exact path={`/expenseManagment`} component={LeftPannel}></Route>
    </div>
  );
}

export default Routing;
