import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import "../Components/Create/Create.css";
import Modal from "../UI/Modal/Modal";
import DeleteProjectStatus from "./Delete/DeleteProjectStatus";
import CloseIcon from "@material-ui/icons/Close";
import EditSubProject from "./Edit/EditSubProject";

function ViewProjectStatus(props) {
  const [rows, setRows] = useState();
  const [showDeleteModal, setShowDeleteModal] = useState(null);
  const [modalDeleteValue, setModalDeleteValue] = useState(null);
  const [showEditModal, setShowEditModal] = useState(null);
  const [modalValue, setModalValue] = useState(null);
  useEffect(() => {
    setRows(props.row);
  }, [props]);
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#FFF1E1",
      color: "#4E4E4E",
      fontWeight: "600",
    },
    body: {
      fontSize: 14,
      borderBottom: 0,
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      position: "relative",
      background: "#F8F8F8 0% 0% no-repeat padding-box",
    },
  }))(TableRow);

  const cancelHandler = () => {
    props.modalClosed(false);
  };

  const useStyles = makeStyles({
    table: {
      width: "100%",
      borderSpacing: "5px",
      borderCollapse: "separate",
    },
    tableContainer: {
      boxShadow: "none",
      minHeight: "45vh",
      maxHeight: "45vh",
      overflowY: "auto",
      width: "100%",
      overflowX: "auto",
      "&::-webkit-scrollbar": {
        width: "5px",
        height: "5px",
        backgroundColor: "white",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#DADADA",
        borderRadius: "10px",
      },
    },
    pagination: {
      position: "absolute",
      top: "90vh",
      zIndex: "100",
      left: "58%",
      width: "40%",
    },
    tableRow: {
      "&:nth-child(1)": {
        borderTopLeftRadius: "4vh",
      },
      "&:nth-last-child(2)": {
        borderTopRightRadius: "4vh",
      },
    },
  });
  const classes = useStyles();

  return (
    <div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <p className="Title">View Sub-Project</p>
        <p
          className="close"
          style={{ marginLeft: "auto" }}
          onClick={cancelHandler}
        >
          <CloseIcon />
        </p>
      </div>
      <TableContainer component={Paper} className={classes.tableContainer}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <StyledTableCell className={classes.tableRow}>
                Manager Remarks
              </StyledTableCell>
              <StyledTableCell className={classes.tableRow}>
                User
              </StyledTableCell>
              <StyledTableCell className={classes.tableRow}>
                User Remarks
              </StyledTableCell>
              <StyledTableCell className={classes.tableRow}>
                Due Date User Task
              </StyledTableCell>
              <StyledTableCell
                className={classes.tableRow}
                style={{ backgroundColor: "white", border: "none" }}
              ></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows && rows.length > 0 ? (
              rows.map((row) => (
                // {rows.map((row) => (
                <StyledTableRow key={row.name}>
                  <StyledTableCell>{row.managerRemarks}</StyledTableCell>
                  <StyledTableCell>{row.user}</StyledTableCell>
                  <StyledTableCell>{row.userRemarks}</StyledTableCell>
                  <StyledTableCell>{row.userTaskDueDate}</StyledTableCell>
                  <StyledTableCell style={{ backgroundColor: "white" }}>
                    <EditOutlinedIcon
                      onClick={() => {
                        setShowEditModal(true);
                        setModalValue(row);
                      }}
                      style={{
                        position: "absolute",
                        top: "50%",
                        right: "2%",
                        width: "20px",
                        height: "20px",
                      }}
                    />
                    <DeleteForeverIcon
                      onClick={() => {
                        setShowDeleteModal(true);
                        setModalDeleteValue(row.id);
                      }}
                      style={{
                        position: "absolute",
                        top: "50%",
                        right: "0%",
                        width: "20px",
                        height: "20px",
                      }}
                    />
                  </StyledTableCell>
                </StyledTableRow>
              ))
            ) : (
              <div className="NoDataFound" style={{ left: "40%" }}>
                No Data Available
              </div>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      {showDeleteModal ? (
        <Modal
          style={{ width: "50%", left: "30%", top: "25%", textAlign: "center" }}
          children={
            <DeleteProjectStatus
              row={modalDeleteValue}
              modalClosed={() => setShowDeleteModal(false)}
            />
          }
          show={showDeleteModal != false}
          modalClosed={() => setShowDeleteModal(false)}
        />
      ) : null}
      {showEditModal ? (
        <Modal
          children={
            <EditSubProject
              row={modalValue}
              users={props.users}
              modalClosed={() => setShowEditModal(false)}
            />
          }
          show={showEditModal != false}
          modalClosed={() => setShowEditModal(false)}
        />
      ) : null}
    </div>
  );
}

export default ViewProjectStatus;
