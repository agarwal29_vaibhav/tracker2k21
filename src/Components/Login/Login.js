import React, { useState, useEffect } from "react";
import "./Login.css";
import {
  Checkbox,
  FormGroup,
  FormControlLabel,
  FormControl,
} from "@material-ui/core";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import axios from "axios";
import Notiflix from "notiflix";

function Login() {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [show, setshow] = useState(false);

  useEffect(()=>{
    if(JSON.parse(localStorage.getItem("LoginDetails"))){
      localStorage.removeItem("LoginDetails");
    }
  },[])

  useEffect(() => {
    document.addEventListener("keyup", handleKeyDown);
    return function cleanup() {
      document.removeEventListener("keyup", handleKeyDown);
    };
  });
  const emailHandler = (e) => {
    setemail(e.target.value);
  };

  const passwordHandler = (e) => {
    setpassword(e.target.value);
  };

  const loginHandler = () => {
    var postbody = {
      user_id: email,
      user_password: password,
    };
    axios
      .post("http://139.59.73.146:9090/v1/api/login", postbody)
      .then((response) => {
        if (response.data.status == "LOGIN_SUCCESSFUL") {
          if (response.data.data.userType === "ADMIN") {
            window.location.href = "/projects";
          } else {
            window.location.href = "/productStatus";
          }
          Notiflix.Notify.success("LOGIN_SUCCESSFUL");
          localStorage.setItem(
            "LoginDetails",
            JSON.stringify(response.data.data)
          );
        } else if (response.data.status !== "LOGIN_SUCCESSFUL") {
          Notiflix.Notify.failure("Incorrect credentials");
        }
      });
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      loginHandler();
    }
  };
  return (
    <div className="row m-0">
      <div
        className="col-6 page"
        style={{
          height: "100vh",
        }}
      ></div>
      <div className="col-6">
        <div className="Login"> LOG IN</div>

        <div class="input-group">
          <label className="label"> Enter Email Address </label>
          <input
            className="input"
            type="text"
            placeholder="abc@gmail.com"
            value={email}
            onChange={(e) => emailHandler(e)}
          />
        </div>
        <div class="input-group">
          <label className="label">Enter Password </label>
          <input
            className="input"
            type={show ? "text" : "password"}
            placeholder="Enter password"
            value={password}
            onChange={(e) => passwordHandler(e)}
          />
        </div>

        <FormControl
          component="fieldset"
          style={{ marginTop: "-5px", marginLeft: "4%" }}
        >
          <FormGroup row>
            <FormControlLabel
              value="showpassword"
              control={
                <Checkbox
                  onClick={() => {
                    setshow(!show);
                  }}
                  color="primary"
                  size="small"
                  style={{ marginLeft: "50px" }}
                />
              }
              label="Show password"
              labelPlacement="end"
              style={{ marginRight: "0px", width: "400px" }}
            />
          </FormGroup>
        </FormControl>
        <button
          type="button"
          className="loginBtn"
          onClick={() => loginHandler()}
        >
          Login
          <span style={{ marginLeft: "10px" }}>
            <ArrowRightAltIcon />
          </span>
        </button>
      </div>
    </div>
  );
}

export default Login;
