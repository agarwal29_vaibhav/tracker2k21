import "./App.css";
import "../node_modules/bootstrap/dist//css/bootstrap.min.css";
import Login from "./Components/Login/Login";
import Routing from "./Components/Routing";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      {
        <Router>
          <Routing />
        </Router>
      }
    </div>
  );
}

export default App;
