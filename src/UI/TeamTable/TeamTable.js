import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import TablePagination from "@material-ui/core/TablePagination";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Dropdown from "../Dropdown/AddToListDropdown";
import Modal from "../Modal/Modal";
import AddIcon from "@material-ui/icons/Add";
import CreateTeam from "../../Components/Create/CreateTeam";
import Header from "../Header/Header";
import EditTeam from "../../Components/Edit/EditTeam";
import DeleteTeam from "../../Components/Delete/DeleteTeam";
import axios from "axios";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../App.css";
import downloadIcon from "../../Assets/download.svg";
import { ClickAwayListener } from "@material-ui/core";
// import { ClickAwayListener } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#FFF1E1",
    color: "#4E4E4E",
    fontWeight: "600",
  },
  body: {
    fontSize: 14,
    borderBottom: 0,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    position: "relative",
    background: "#F8F8F8 0% 0% no-repeat padding-box",
  },
}))(TableRow);

function createData(id, name, email, login, password, hourlyRate, Status) {
  return { id, name, email, login, password, hourlyRate, Status };
}

const useStyles = makeStyles({
  table: {
    width: "100%",
    borderSpacing: "5px",
    borderCollapse: "separate",
  },
  tableContainer: {
    boxShadow: "none",
    minHeight: "68vh",
    maxHeight: "68vh",
    overflowY: "auto",
    width: "100%",
    overflowX: "auto",
    "&::-webkit-scrollbar": {
      width: "10px",
      height: "15px",
      backgroundColor: "white",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#DADADA",
      borderRadius: "10px",
    },
  },
  pagination: {
    bottom: "4rem",
    right: "0",
    width: "40%",
    zIndex: "1",
    position: "absolute",
  },
  tableRow: {
    "&:first-child": {
      borderTopLeftRadius: "4vh",
    },
    "&:nth-last-child(2)": {
      borderTopRightRadius: "4vh",
    },
  },
});

export default function TeamTable() {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const [openModal, setopenModal] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [name, setname] = useState(false);

  const [Email, setEmail] = useState({ show: false });
  const [LoginId, setLoginId] = useState(false);
  const [Rate, setRate] = useState(false);
  const [status, setstatus] = useState(false);
  const [NameDropdown, setNameDropdown] = useState([]);
  const statusDropdown = ["ACTIVE", "INACTIVE", "INVALID"];
  const [RowData, setRowData] = useState([]);
  const [RowDataRef, setRowDataRef] = useState([]);

  const [showEditModal, setShowEditModal] = useState(null);
  const [modalValue, setModalValue] = useState(null);
  const [showDeleteModal, setShowDeleteModal] = useState(null);
  const [modalDeleteValue, setModalDeleteValue] = useState(null);
  const [rows, setRows] = useState(null);
  const [showLoader, setShowLoader] = useState(false);
  const [checkedNameList, setCheckedNameList] = useState([]);
  const [checkedStatusList, setcheckedStatusList] = useState([]);

  useEffect(() => {
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/team")
      .then((res) => {
        setNameDropdown(
          res.data.data[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    setShowLoader(true);
    axios
      .get("http://139.59.73.146:9090/v1/api/show/team")
      .then((res) => {
        setRowData(res.data.data);
        setRowDataRef(res.data.data);
        setShowLoader(false);
      })
      .catch((err) => {
        console.log(err);
        setShowLoader(false);
      });
  }, []);

  useEffect(() => {
    setRows(() => {
      const a = RowData.map((x) => {
        return createData(
          x.id,
          x.name,
          x.emailId,
          x.userId,
          x.userPassword,
          x.hourlyRate,
          x.status
        );
      });
      return a;
    });
  }, [RowData]);

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  if (!loginObj || loginObj === null) {
    window.location.href = "/";
  }
  const exportData = () => {
    let data = rows.map((row) => {
      return [
        row.name,
        row.email,
        row.login,
        row.password,
        row.hourlyRate,
        row.Status,
      ];
    });

    data.splice(0, 0, [
      "Name",
      "Email Id",
      "LoginId",
      "Password",
      "Hourly Rate",
      "Status",
    ]);

    let csvContent =
      "data:text/csv;charset=utf-8," + data.map((e) => e.join(",")).join("\n");

    /* create a hidden <a> DOM node and set its download attribute */
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "Teams.csv");
    document.body.appendChild(link);
    /* download the data file */
    link.click();
  };

  const handleClickAway = () => {
    alert("clicked");
  };
  return (
    <div>
      <Header userName="user" title="Team" />

      <button onClick={() => setopenModal(true)} className="CreateProject">
        <AddIcon style={{ color: "black" }} /> Create Team
      </button>
      <button onClick={exportData} className="DownloadBtn">
        <img src={downloadIcon} /> Download
      </button>
      {openModal ? (
        <Modal
          children={<CreateTeam setopenModal={setopenModal} />}
          show={openModal != false}
          modalClosed={() => setopenModal(false)}
        />
      ) : null}

      {showLoader ? (
        <div className="loader">
          {" "}
          <CircularProgress />
        </div>
      ) : (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            stickyHeader="true"
            aria-label="sticky table"
            className={classes.table}
          >
            {/* <Box> */}
            <TableHead>
              <TableRow>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Name
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setname((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {name.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setname((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedNameList}
                          setDefaultValue={setCheckedNameList}
                          dropdownClass=" dropdown teamName"
                          wholeList={NameDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"name"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "20vw" }}
                >
                  Email Id
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  LoginId
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Password
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Hourly Rate
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Status
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setstatus((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {status.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setstatus((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedStatusList}
                          setDefaultValue={setcheckedStatusList}
                          dropdownClass=" dropdown statusDropdown"
                          wholeList={statusDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"status"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  style={{ backgroundColor: "white", border: "none" }}
                ></StyledTableCell>
              </TableRow>
            </TableHead>
            {/* </Box> */}
            <TableBody>
              {/* <Box style={{background:'yellow'}}> */}

              {rows && rows.length > 0 ? (
                rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => (
                    <StyledTableRow key={row.name}>
                      <StyledTableCell component="th" scope="row">
                        {row.name}
                      </StyledTableCell>
                      <StyledTableCell>{row.email}</StyledTableCell>
                      <StyledTableCell>{row.login}</StyledTableCell>
                      <StyledTableCell>{row.password}</StyledTableCell>
                      <StyledTableCell>{row.hourlyRate}</StyledTableCell>
                      <StyledTableCell style={{ textTransform: "uppercase" }}>
                        {row.Status}
                      </StyledTableCell>
                      <StyledTableCell style={{ backgroundColor: "white" }}>
                        <EditOutlinedIcon
                          onClick={() => {
                            setShowEditModal(true);
                            setModalValue(row);
                          }}
                          style={{
                            position: "absolute",
                            top: "50%",
                            right: "1.25%",
                            width: "20px",
                            height: "20px",
                            cursor: "pointer",
                          }}
                        />
                        <DeleteForeverIcon
                          onClick={() => {
                            setShowDeleteModal(true);
                            setModalDeleteValue(row.id);
                          }}
                          style={{
                            position: "absolute",
                            top: "50%",
                            right: "0%",
                            width: "20px",
                            height: "20px",
                            cursor: "pointer",
                          }}
                        />
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
              ) : (
                <div className="NoDataFound">No Data Available</div>
              )}

              {/* </Box> */}
            </TableBody>
          </Table>
          <TablePagination
            className={classes.pagination}
            rowsPerPageOptions={[5, 10, 15, 20]}
            component="div"
            count={rows ? rows.length : 0}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </TableContainer>
      )}

      {showEditModal ? (
        <Modal
          children={
            <EditTeam
              row={modalValue}
              modalClosed={() => setShowEditModal(false)}
            />
          }
          show={showEditModal != false}
          modalClosed={() => setShowEditModal(false)}
        />
      ) : null}

      {showDeleteModal ? (
        <Modal
          style={{
            width: "50%",
            left: "30%",
            top: "25%",
            textAlign: "center",
          }}
          children={
            <DeleteTeam
              row={modalDeleteValue}
              modalClosed={() => setShowDeleteModal(false)}
            />
          }
          show={showDeleteModal != false}
          modalClosed={() => setShowDeleteModal(false)}
        />
      ) : null}
    </div>
  );
}
