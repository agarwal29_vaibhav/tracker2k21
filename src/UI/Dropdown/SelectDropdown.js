import React, { useState, useEffect } from "react";
import {
  List,
  ListItem,
  Checkbox,
  FormControl,
  FormControlLabel,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CheckIcon from "@material-ui/icons/Check";
import CancelOutlinedIcon from "@material-ui/icons/CancelOutlined";

const useStyles = makeStyles({
  dropdown: {
    textAlign: "left",
    font: "normal normal 600 12px/17px Open Sans !important",
    letterSpacing: "0px",
    color: "#808080",
    opacity: "1",
    textTransform: "capitalize",
    position: "absolute",
    left: "0",
  },
  listItem: {
    padding: "4px 16px",
    "& label": {
      marginRight: "0",
    },
  },
  inputField: {
    font: "normal normal normal 12px/17px Open Sans",
    letterSpacing: "0px",
    color: "#000000",
    width: "85%",
    marginLeft: "5%",
    marginTop: "2%",
    marginRight: "1%",
    padding: "0.125rem",
    "&::placeholder": {
      fontSize: "11px",
      color: "grey",
      opacity: "0.5",
    },
  },
  addNew: {
    textAlign: "left",
    font: "normal normal 600 12px/17px Open Sans !important",
    letterSpacing: "0px",
    color: "#0072C6",
    opacity: "1",
    padding: "3.5px 0px",
    textTransform: "capitalize",
  },
  noData: {
    textAlign: "center",
    font: "normal normal normal 12px/17px Open Sans",
    letterSpacing: "0px",
    color: "#767676",
    padding: "0px 8px",
    margin: "0",
  },
  list: {
    background: "#fff",
    borderRadius: "0px 10px 10px 10px",
    cursor: "pointer",
  },
});
function SelectDropdown(props) {
  const classes = useStyles();
  const [list, setlist] = useState([]);
  const [inputVal, setInputVal] = useState("");

  useEffect(() => {
    let arr = [];
    // if (props.project) {
    //   props.wholeList &&
    //     props.wholeList.forEach((el) => {
    //       arr.push(el);
    //     });
    //   setlist(arr);
    // } else {
    if (props.wholeList) {
      setlist(props.wholeList);
    }
    // }
  }, [props.wholeList]);

  const searchFunc = (searchVal) => {
    let arr = [];
    props.wholeList &&
      props.wholeList.map((el) => {
        // if (props.project) {
        //   if (el[0].toLowerCase().includes(searchVal.toLowerCase())) {
        //     arr.push(el[0]);
        //   }
        // } else {
        if (el.toLowerCase().includes(searchVal.toLowerCase())) {
          arr.push(el);
        }
        // }
      });
    setlist(arr);
  };

  const cancelFunc = () => {
    setInputVal("");
    let arr = [];
    // if (props.project) {
    //   props.wholeList &&
    //     props.wholeList.forEach((el) => {
    //       arr.push(el[0]);
    //     });
    //   setlist(arr);
    // } else {
    setlist(props.wholeList);
    // }
  };

  return (
    <div
      className={props.dropdownClass ? props.dropdownClass : classes.dropdown}
    >
      <FormControl style={{ width: "100%" }}>
        <div className={classes.addNewDiv}>
          {props.showInput ? (
            <div>
              <input
                className={classes.inputField}
                placeholder="Search here"
                value={inputVal}
                type="text"
                onChange={(e) => {
                  setInputVal(e.target.value);
                  searchFunc(e.target.value);
                }}
              />
              <CancelOutlinedIcon
                style={{ width: "13px", height: "13px", cursor: "pointer" }}
                onClick={() => cancelFunc()}
              />
            </div>
          ) : null}
        </div>
        <List className={classes.list}>
          {list && list.length > 0 ? (
            list.map((data1, index) => (
              <ListItem
                key={data1}
                value={data1}
                className={classes.listItem}
                onClick={() => props.onChange(data1)}
              >
                {data1}
              </ListItem>
            ))
          ) : (
            <p className={classes.noData}>{props.noDataLabel}</p>
          )}
        </List>
      </FormControl>
    </div>
  );
}

export default SelectDropdown;
