import React, { useState, useEffect } from "react";
import {
  List,
  ListItem,
  Checkbox,
  FormControl,
  FormControlLabel,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CheckIcon from "@material-ui/icons/Check";
import CancelOutlinedIcon from "@material-ui/icons/CancelOutlined";

const useStyles = makeStyles({
  dropdown: {
    textAlign: "left",
    font: "normal normal 600 12px/17px Open Sans !important",
    letterSpacing: "0px",
    color: "#808080",
    opacity: "1",
    textTransform: "capitalize",
  },
  listItem: {
    padding: "0 16px",
    "& label": {
      marginRight: "0",
    },
  },
  inputField: {
    border: " none !important",
    font: "normal normal normal 12px/17px Open Sans",
    letterSpacing: "0px",
    color: "#000000",
    width: "85%",
    marginLeft: "5%",
    marginTop: "2%",
    marginRight: "1%",
    padding: "0.125rem",
    "&::placeholder": {
      fontSize: "11px",
      color: "grey",
      opacity: "0.5",
    },
  },
  addNew: {
    textAlign: "left",
    font: "normal normal 600 12px/17px Open Sans !important",
    letterSpacing: "0px",
    color: "#0072C6",
    opacity: "1",
    padding: "3.5px 0px",
    textTransform: "capitalize",
  },
  noData: {
    textAlign: "center",
    font: "normal normal normal 12px/17px Open Sans",
    letterSpacing: "0px",
    color: "#767676",
    padding: "0px 8px",
    margin:"0"
  },
  list: {
    background: "#FFE4C0 0% 0% no-repeat padding-box",
    borderRadius: "0px 10px 10px 10px",
    margin: "0.25rem 0.5rem",
  },
  checkboxStyle: {
    background: "#FFFFFF 0% 0% no-repeat padding-box",
    border: "0.20000000298023224px solid #000000",
    width: "14px",
    height: "14px",
    position: "relative",
  },
  checkIcon: {
    width: "14px",
    height: "14px",
    position: "absolute",
    left: "-1px",
    top: "-1px",
  },
});
function Dropdown(props) {
  const classes = useStyles();
  const [list, setlist] = useState([]);
  const [inputVal, setInputVal] = useState("");
  const [checked, setChecked] = useState(null);

  useEffect(() => {
    setlist(props.wholeList);
    props.wholeList &&
      props.wholeList.map((item) => {
        if (props.defaultValue && props.defaultValue.includes(item)) {
          setChecked((prev) => {
            return { ...prev, [item]: true };
          });
        } else {
          setChecked((prev) => {
            return { ...prev, [item]: false };
          });
        }
      });
  }, [props.wholeList]);

  const filterData = (filterdt) => {
    if (filterdt.length > 0) {
      const filtered = [];
      props.refdata.forEach((val) => {
        filterdt.filter((item) => {
          var key = props.type;
          if (
            item.toString().toLowerCase() == val[key].toString().toLowerCase()
          ) {
            filtered.push(val);
          }
        });
      });
      props.setrefdata(filtered);
    } else {
      props.setrefdata(props.refdata);
    }
  };

  const handleChange = (e, data) => {
    if (props.multiple) {
      list &&
        list.map((item) => {
          if (item.localeCompare(data) == 0) {
            setChecked((prev) => {
              return { ...prev, [item]: !prev[item] };
            });
          }
        });
      if (props.defaultValue.includes(data)) {
        props.setDefaultValue((prev) => {
          let newArray = [...prev];
          let index = newArray.indexOf(data);
          newArray.splice(index, 1);
          filterData(newArray);

          return newArray;
        });
      } else {
        props.setDefaultValue((prev) => [...prev, data]);

        let nwdt = props.defaultValue;
        nwdt.push(data);
        filterData(nwdt);
      }
      // props.onChange(e.target.value);
    } else {
      list &&
        list.map((item) => {
          if (item.localeCompare(data) == 0) {
            setChecked((prev) => {
              return { ...prev, [item]: true };
            });
          } else {
            setChecked((prev) => {
              return { ...prev, [item]: false };
            });
          }
        });
      props.onChange(e.target.value);
    }
  };

  const searchFunc = (searchVal) => {
    let arr = [];
    props.wholeList &&
      props.wholeList.map((el) => {
        if (el.toLowerCase().includes(searchVal.toLowerCase())) {
          arr.push(el);
        }
      });
    setlist(arr);
  };

  const cancelFunc = () => {
    setInputVal("");
    setlist(props.wholeList);
  };

  return (
    <div
      className={props.dropdownClass ? props.dropdownClass : classes.dropdown}
    >
      <FormControl style={{ width: "100%" }}>
        <div className={classes.addNewDiv}>
          {props.showInput ? (
            <div>
              <input
                className={classes.inputField}
                placeholder="Search here"
                value={inputVal}
                type="text"
                onChange={(e) => {
                  setInputVal(e.target.value);
                  searchFunc(e.target.value);
                }}
              />
              <CancelOutlinedIcon
                style={{ width: "13px", height: "13px" , cursor:"pointer"}}
                onClick={() => cancelFunc()}
              />
            </div>
          ) : null}
        </div>
        <List className={classes.list}>
          {list.length > 0 ? (
            list.map((data1, index) => (
              <ListItem key={data1} value={data1} className={classes.listItem}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checkedIcon={
                        <span className={classes.checkboxStyle}>
                          <CheckIcon className={classes.checkIcon} />
                        </span>
                      }
                      icon={<span className={classes.checkboxStyle} />}
                      value={data1}
                      checked={checked ? checked[data1] : false}
                      onChange={(e) => handleChange(e, data1)}
                      name="check"
                    />
                  }
                  label={<div className={classes.dropdown}>{data1}</div>}
                />
              </ListItem>
            ))
          ) : (
            <p className={classes.noData}>{props.noDataLabel}</p>
          )}
        </List>
      </FormControl>
    </div>
  );
}

export default Dropdown;
