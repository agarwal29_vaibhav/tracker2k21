import React from "react";
import "./Header.css";
import logout from "../../Assets/logout.png";

function Header(props) {
  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  return (
    <div className="header">
      <p className="headerHello">
        Hello <span style={{ color: "#FF9414" }}>{loginObject.userId}</span>
        &nbsp;!
      </p>
      <p className="headerTitle">{props.title}</p>
      <div
        className="logoutbtn"
        onClick={() => {
          localStorage.removeItem("LoginDetails");
          window.location.replace("/");
        }}
      >
        Log Out
        <img
          src={logout}
          style={{
            height: "18px",
            width: "18px",
            marginLeft: "7%",
            marginBottom: "3%",
          }}
        />
      </div>
    </div>
  );
}

export default Header;
