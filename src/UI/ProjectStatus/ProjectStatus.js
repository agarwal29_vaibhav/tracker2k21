import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import TablePagination from "@material-ui/core/TablePagination";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Dropdown from "../Dropdown/AddToListDropdown";
import Calender from "../Calender/CalenderDropdown";
import Time from "../Calender/TimeDropdown";
import Modal from "../Modal/Modal";
import Header from "../Header/Header";
import EditProjectStatus from "../../Components/Edit/EditProjectStatus";
import axios from "axios";
import downloadIcon from "../../Assets/download.svg";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import VisibilityIcon from "@material-ui/icons/Visibility";
import "../../App.css";
import moment from "moment";
import CreateProjectStatus from "../../Components/Create/CreateProjectStatus";
import Notiflix from "notiflix";
import ViewProjectStatus from "../../Components/ViewProjectStatus";
import { ClickAwayListener } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#FFF1E1",
    color: "#4E4E4E",
    fontWeight: "600",
  },
  body: {
    fontSize: 14,
    borderBottom: 0,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    position: "relative",
    background: "#F8F8F8 0% 0% no-repeat padding-box",
  },
}))(TableRow);

function createData(
  id,
  project,
  client,
  manager,
  managerremark,
  user,
  userremark,
  dueDateuser,
  dueDateproject,
  hourspent,
  costspent,
  status,
  color
) {
  return {
    id,
    project,
    client,
    manager,
    managerremark,
    user,
    userremark,
    dueDateuser,
    dueDateproject,
    hourspent,
    costspent,
    status,
    color,
  };
}

const useStyles = makeStyles({
  table: {
    width: "100%",
    borderSpacing: "5px",
    borderCollapse: "separate",
  },
  tableContainer: {
    boxShadow: "none",
    minHeight: "68vh",
    maxHeight: "68vh",
    overflowY: "auto",
    width: "100%",
    overflowX: "auto",
    "&::-webkit-scrollbar": {
      width: "10px",
      height: "15px",
      backgroundColor: "white",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#DADADA",
      borderRadius: "10px",
    },
  },
  pagination: {
    bottom: "4rem",
    right: "0",
    width: "40%",
    zIndex: "1",
    position: "absolute",
  },
  tableRow: {
    "&:nth-child(2)": {
      borderTopLeftRadius: "4vh",
    },
    "&:nth-last-child(2)": {
      borderTopRightRadius: "4vh",
    },
  },
});

export default function ProjectStatus() {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [project, setproject] = useState(false);
  const [Client, setClient] = useState({ show: false });
  const [Manager, setManager] = useState(false);
  const [User, setUser] = useState(false);
  const [dueDate, setdueDate] = useState(false);
  const [dueDateProject, setdueDateProject] = useState(false);
  const [hourSpent, sethourSpent] = useState(false);
  const [costSpent, setcostSpent] = useState(false);
  const [status, setstatus] = useState(false);

  const [showEditModal, setShowEditModal] = useState(null);
  const [modalValue, setModalValue] = useState(null);
  const [showCreateProjectModal, setShowCreateProjectModal] = useState(null);
  const [modalCreateProjectValue, setModalCreateProjectValue] = useState(null);
  const [viewProjectModal, setViewProjectModal] = useState(null);
  const [modalViewValue, setModalViewValue] = useState(null);
  const [RowData, setRowData] = useState([]);
  const [RowDataRef, setRowDataRef] = useState([]);

  const [rows, setRows] = useState(null);
  const [showLoader, setShowLoader] = useState(false);
  const [ClientDropdownlist, setClientDropdownlist] = useState([]);

  const [ProjectDropdown, setProjectDropdown] = useState([]);
  const [ManagerList, setManagerList] = useState([]);
  const statusDropdown = ["OPEN", "CLOSED", "PAUSED"];

  const [checkedProjectList, setCheckedProjectList] = useState([]);
  const [checkedClientList, setCheckedClientList] = useState([]);
  const [checkedManagerAssignList, setCheckedManagerAssignList] = useState([]);
  const [checkedStatusList, setCheckedStatusList] = useState([]);
  const [DueUserdropdownDate, setDueUserDropdownDate] = useState([null, null]);
  const [DueProjectdropdownDate, setDueProjectDropdownDate] = useState([
    null,
    null,
  ]);

  const exportData = () => {
    let data = rows.map((row) => {
      return [
        row.project,
        row.client,
        row.manager,
        row.managerremark,
        row.user,
        row.userremark,
        row.dueDateuser,
        row.dueDateproject,
        row.hourspent,
        row.costspent,
        row.status,
        row.color,
      ];
    });

    data.splice(0, 0, [
      "Project",
      "Client",
      "Manager",
      "Manager Remarks",
      "User",
      "User Remarks",
      "Due Date User Task",
      "Due Date Project",
      "Hours Spent",
      "Cost Spent",
      "Status",
    ]);

    let csvContent =
      "data:text/csv;charset=utf-8," + data.map((e) => e.join(",")).join("\n");

    /* create a hidden <a> DOM node and set its download attribute */
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "ProjectStatus.csv");
    document.body.appendChild(link);
    /* download the data file */
    link.click();
  };

  const showSubProject = (id) => {
    axios
      .post("http://139.59.73.146:9090/v1/api/show/sub/project", {
        project_id: id,
      })
      .then((response) => {
        if (response.data.status === "FETCHING_SUCCESSFUL") {
          setModalViewValue(response.data.data);
          setViewProjectModal(true);
        }
      });
  };

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  if (!loginObj || loginObj === null) {
    window.location.href = "/";
  }

  useEffect(() => {
    setShowLoader(true);

    axios
      .get("http://139.59.73.146:9090/v1/api/my/update")
      .then((res) => {})
      .catch((err) => {
        console.log(err);
      });

    var loginObj = localStorage.getItem("LoginDetails");
    var loginObject = JSON.parse(loginObj);
    let is_admin;
    if (loginObject.userType == "ADMIN") {
      is_admin = "yes";
    } else {
      is_admin = "no";
    }
    let manager_assigned = loginObject.name;
    axios
      .post("http://139.59.73.146:9090/v1/api/show/project", {
        manager_assigned: manager_assigned,
        is_admin: is_admin,
      })
      .then((res) => {
        setRowData(res.data.data);
        setRowDataRef(res.data.data);
        setShowLoader(false);
      })
      .catch((err) => {
        console.log(err);
        setShowLoader(false);
      });
  }, []);

  useEffect(() => {
    setRows(() => {
      const a = RowData.map((row) => {
        return createData(
          row.id,
          row.name,
          row.client,
          row.managerAssigned,
          row.managerRemarks == null ? "NA" : row.managerRemarks,
          row.user == null ? "NA" : row.user,
          row.userRemarks == null ? "NA" : row.userRemarks,
          row.userTaskDueDate == null ? "NA" : row.userTaskDueDate,
          moment(row.dueDate).format("ll"),
          row.hoursSpent == null ? "NA" : row.hoursSpent,
          row.costSpent == null ? "NA" : row.costSpent,
          row.status,
          row.color
        );
      });
      return a;
    });
  }, [RowData]);

  useEffect(() => {
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/project")
      .then((res) => {
        let arr = [];
        res.data.data[0].map((x, index) => {
          if (!arr.includes(x[0])) {
            arr.push(x[0]);
          }
        });
        setProjectDropdown(arr);
      })
      .catch((err) => {
        console.log(err);
      });

    // client dropdown
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/client")
      .then((res) => {
        setClientDropdownlist(
          res.data.data[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });

    //manager dropdown
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/team")
      .then((res) => {
        setManagerList(
          res.data.data[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <Header userName="user" title="Project Status" />

      <button
        onClick={exportData}
        className="DownloadBtn"
        style={{ marginBottom: "1rem" }}
      >
        <img src={downloadIcon} /> Download
      </button>
      {showLoader ? (
        <div className="loader">
          {" "}
          <CircularProgress />
        </div>
      ) : (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            className={classes.table}
            stickyHeader
            aria-label="sticky table"
          >
            <TableHead>
              <TableRow>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{
                    backgroundColor: "white",
                    border: "none",
                    padding: "4px",
                    width: "0.01rem",
                  }}
                ></StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Project
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setproject((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {project.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setproject((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedProjectList}
                          setDefaultValue={setCheckedProjectList}
                          dropdownClass=" dropdown projectDropdown"
                          wholeList={ProjectDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"name"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Client
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setClient((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {Client.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setClient((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedClientList}
                          setDefaultValue={setCheckedClientList}
                          dropdownClass=" dropdown clientDropdown"
                          wholeList={ClientDropdownlist}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"client"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Manager
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setManager((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {Manager.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setManager((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedManagerAssignList}
                          setDefaultValue={setCheckedManagerAssignList}
                          dropdownClass="dropdown managerDropdown"
                          wholeList={ManagerList}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"managerAssigned"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Due Date Project
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setdueDateProject((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {dueDateProject.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setdueDateProject((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Calender
                          dropdownClass=" dropdown closingDropdown"
                          date={DueProjectdropdownDate}
                          setDate={setDueProjectDropdownDate}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          setRowData={setRowDataRef}
                          valueKey={"dueDate"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "10vw" }}
                >
                  Hours Spent
                </StyledTableCell>

                {loginObject.userType == "ADMIN" ? (
                  <StyledTableCell
                    className={classes.tableRow}
                    style={{ width: "10vw" }}
                  >
                    Cost Spent
                  </StyledTableCell>
                ) : null}

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Status
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setstatus((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {status.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setstatus((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedStatusList}
                          setDefaultValue={setCheckedStatusList}
                          dropdownClass="dropdown statusDropdown"
                          wholeList={statusDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"status"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ backgroundColor: "white", border: "none" }}
                ></StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows && rows.length > 0 ? (
                rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => (
                    // {rows.map((row) => (

                    <StyledTableRow
                      key={row.name}
                      style={{
                        backgroundColor:
                          row.status == "CLOSED" ? "lightgreen" : row.color,
                      }}
                    >
                      <StyledTableCell
                        style={{
                          backgroundColor: "white",
                          border: "none",
                          padding: "4px",
                          width: "0.01rem",
                        }}
                      >
                        <AddCircleIcon
                          onClick={() => {
                            setShowCreateProjectModal(true);
                            setModalCreateProjectValue(row.id);
                          }}
                          style={{
                            position: "absolute",
                            top: "0%",
                            left: "0.2%",
                            width: "1rem",
                            height: "1rem",
                            color: "rgb(255, 148, 20)",
                          }}
                        />
                      </StyledTableCell>
                      <StyledTableCell component="th" scope="row">
                        {row.project}
                      </StyledTableCell>
                      <StyledTableCell>{row.client}</StyledTableCell>
                      <StyledTableCell>{row.manager}</StyledTableCell>
                      <StyledTableCell>{row.dueDateproject}</StyledTableCell>
                      <StyledTableCell>{row.hourspent}</StyledTableCell>
                      {loginObject.userType == "ADMIN" ? (
                        <StyledTableCell>{row.costspent}</StyledTableCell>
                      ) : null}
                      <StyledTableCell style={{ textTransform: "uppercase" }}>
                        {row.status}
                      </StyledTableCell>

                      <StyledTableCell
                        style={{ backgroundColor: "white", width: "1vw" }}
                      >
                        <EditOutlinedIcon
                          onClick={() => {
                            setShowEditModal(true);
                            setModalValue(row);
                          }}
                          style={{
                            position: "absolute",
                            top: "50%",
                            right: "1%",
                            width: "20px",
                            height: "20px",
                            cursor: "pointer",
                          }}
                        />
                        <VisibilityIcon
                          onClick={() => showSubProject(row.id)}
                          style={{
                            position: "absolute",
                            top: "30%",
                            left: "0.2%",
                            width: "1rem",
                            height: "1rem",
                            color: "rgb(255, 148, 20)",
                            cursor: "pointer",
                          }}
                        />
                        {/*<DeleteForeverIcon
                          style={{
                            position: "absolute",
                            top: "50%",
                            right: "0%",
                            width: "20px",
                            height: "20px",
                          }}
                        />*/}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
              ) : (
                <div className="NoDataFound">No Data Available</div>
              )}
            </TableBody>
          </Table>
          <TablePagination
            className={classes.pagination}
            rowsPerPageOptions={[5, 10, 15, 20]}
            component="div"
            count={rows ? rows.length : 0}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </TableContainer>
      )}
      {showEditModal ? (
        <Modal
          children={
            <EditProjectStatus
              row={modalValue}
              modalClosed={() => setShowEditModal(false)}
            />
          }
          show={showEditModal != false}
          modalClosed={() => setShowEditModal(false)}
        />
      ) : null}
      {showCreateProjectModal ? (
        <Modal
          children={
            <CreateProjectStatus
              row={modalCreateProjectValue}
              users={ManagerList}
              modalClosed={() => setShowCreateProjectModal(false)}
            />
          }
          show={showCreateProjectModal != false}
          modalClosed={() => setShowCreateProjectModal(false)}
        />
      ) : null}
      {viewProjectModal ? (
        <Modal
          style={{ overflow: "hidden" }}
          children={
            <ViewProjectStatus
              row={modalViewValue}
              users={ManagerList}
              modalClosed={() => setViewProjectModal(false)}
            />
          }
          show={viewProjectModal != false}
          modalClosed={() => setViewProjectModal(false)}
        />
      ) : null}
    </div>
  );
}
