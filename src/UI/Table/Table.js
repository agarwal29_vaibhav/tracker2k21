import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import TablePagination from "@material-ui/core/TablePagination";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Dropdown from "../Dropdown/AddToListDropdown";
import Calender from "../Calender/CalenderDropdown";
import Time from "../Calender/TimeDropdown";
import Cost from "../Calender/CostDropdown";
import "./Table.css";
import Modal from "../Modal/Modal";
import AddIcon from "@material-ui/icons/Add";
import CreateProject from "../../Components/Create/CreateProject";
import Header from "../Header/Header";
import EditProject from "../../Components/Edit/EditProject";
import DeleteProject from "../../Components/Delete/DeleteProject";
import axios from "axios";
import downloadIcon from "../../Assets/download.svg";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../App.css";
import Notification from "../../Components/NotificationModal";
import moment from "moment";
import { ClickAwayListener } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#FFF1E1",
    color: "#4E4E4E",
    fontWeight: "600",
  },
  body: {
    fontSize: 14,
    borderBottom: 0,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    position: "relative",
    background: "#F8F8F8 0% 0% no-repeat padding-box",
  },
}))(TableRow);

function createData(
  id,
  project,
  client,
  billable,
  manager,
  creationdate,
  duedate,
  closingdate,
  hoursspent,
  costspent,
  status
) {
  return {
    id,
    project,
    client,
    billable,
    manager,
    creationdate,
    duedate,
    closingdate,
    hoursspent,
    costspent,
    status,
  };
}

const useStyles = makeStyles({
  table: {
    width: "100%",
    borderSpacing: "5px",
    borderCollapse: "separate",
  },
  tableContainer: {
    boxShadow: "none",
    minHeight: "68vh",
    maxHeight: "68vh",
    overflowY: "auto",
    width: "100%",
    overflowX: "auto",
    "&::-webkit-scrollbar": {
      width: "10px",
      height: "15px",
      backgroundColor: "white",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#DADADA",
      borderRadius: "10px",
    },
  },
  pagination: {
    bottom: "4rem",
    right: "0",
    width: "40%",
    zIndex: "1",
    position: "absolute",
  },
  tableRow: {
    "&:first-child": {
      borderTopLeftRadius: "4vh",
    },
    "&:nth-last-child(2)": {
      borderTopRightRadius: "4vh",
    },
  },
});

export default function CustomizedTables() {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const [openModal, setopenModal] = useState(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [project, setproject] = useState(false);
  const [Client, setClient] = useState({ show: false });
  const [Billable, setBillable] = useState(false);
  const [ManagerAssignmed, setManagerAssignmed] = useState(false);
  const [creationDate, setcreationDate] = useState(false);
  const [dueDate, setdueDate] = useState(false);
  const [closingDate, setclosingDate] = useState(false);
  const [hourSpent, sethourSpent] = useState(false);
  const [costSpent, setcostSpent] = useState(false);
  const [status, setstatus] = useState(false);
  const [ClientDropdownlist, setClientDropdownlist] = useState([]);
  const [ProjectDropdown, setProjectDropdown] = useState([]);
  const BillableDropdown = ["true", "false"];
  const [ManagerAssignmedDropdown, setManagerAssignmedDropdown] = useState([]);
  const statusDropdown = ["Opened", "Closed", "Paused"];
  const [showEditModal, setShowEditModal] = useState(null);
  const [modalValue, setModalValue] = useState(null);
  const [showDeleteModal, setShowDeleteModal] = useState(null);
  const [modalDeleteValue, setModalDeleteValue] = useState(null);
  const [RowData, setRowData] = useState([]);
  const [RowDataRef, setRowDataRef] = useState([]);

  const [rows, setRows] = useState(null);
  const [showLoader, setShowLoader] = useState(false);
  const [checkedProjectList, setCheckedProjectList] = useState([]);
  const [checkedClientList, setCheckedClientList] = useState([]);
  const [checkedBillableList, setCheckedBillableList] = useState([]);
  const [checkedManagerAssignList, setCheckedManagerAssignList] = useState([]);
  const [checkedStatusList, setCheckedStatusList] = useState([]);
  const [CreationdropdownDate, setCreationDropdownDate] = useState([
    null,
    null,
  ]);
  const [DuedropdownDate, setDueDropdownDate] = useState([null, null]);
  const [ClosingdropdownDate, setClosingDropdownDate] = useState([null, null]);

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  if (!loginObj || loginObj === null) {
    window.location.href = "/";
  }

  useEffect(() => {
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/project")
      .then((res) => {
        let arr = [];
        res.data.data[0].map((x, index) => {
          if (!arr.includes(x[0])) {
            arr.push(x[0]);
          }
        });
        setProjectDropdown(arr);
      })
      .catch((err) => {
        console.log(err);
      });

    // client dropdown
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/client")
      .then((res) => {
        setClientDropdownlist(
          res.data.data[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });

    //manager dropdown
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/team")
      .then((res) => {
        setManagerAssignmedDropdown(
          res.data.data[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    setShowLoader(true);
    var loginObj = localStorage.getItem("LoginDetails");
    var loginObject = JSON.parse(loginObj);
    let is_admin;
    if (loginObject.userType == "ADMIN") {
      is_admin = "yes";
    } else {
      is_admin = "no";
    }
    let manager_assigned = loginObject.name;
    axios
      .post("http://139.59.73.146:9090/v1/api/show/project", {
        manager_assigned: manager_assigned,
        is_admin: is_admin,
      })
      .then((res) => {
        setRowData(res.data.data);
        setRowDataRef(res.data.data);
        setShowLoader(false);
      })
      .catch((err) => {
        console.log(err);
        setShowLoader(false);
      });
  }, []);

  useEffect(() => {
    setRows(() => {
      const a = RowData.map((x) => {
        return createData(
          x.id,
          x.name,
          x.client,
          x.billable,
          x.managerAssigned,
          x.creationDate,
          x.dueDate,
          x.closingDate == null
            ? "Yet to be closed"
            : moment(x.closingDate).format("ll"),
          x.hoursSpent == null ? "NA" : x.hoursSpent,
          x.costSpent == null ? "NA" : x.costSpent,
          x.status
        );
      });
      return a;
    });
  }, [RowData]);

  const exportData = () => {
    let data = rows.map((row) => {
      return [
        row.project,
        row.client,
        row.billable,
        row.manager,
        row.creationdate,
        row.duedate,
        row.closingdate,
        row.hoursspent,
        row.costspent,
        row.status,
      ];
    });

    data.splice(0, 0, [
      "Project",
      "Client",
      "Billable",
      "Manager Assigned",
      "Creation Date",
      "Due Date",
      "Closing Date",
      "Hours Spent",
      "Cost Spent",
      "Status",
    ]);

    let csvContent =
      "data:text/csv;charset=utf-8," + data.map((e) => e.join(",")).join("\n");

    /* create a hidden <a> DOM node and set its download attribute */
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "Project.csv");
    document.body.appendChild(link);
    /* download the data file */
    link.click();
  };

  return (
    <React.Fragment>
      <Header userName="user" title="Project" />

      <button onClick={() => setopenModal(true)} className="CreateProject">
        <AddIcon style={{ color: "black" }} /> Create Project
      </button>
      <button onClick={exportData} className="DownloadBtn">
        <img src={downloadIcon} /> Download
      </button>
      {openModal ? (
        <Modal
          children={
            <CreateProject
              setopenModal={setopenModal}
              managerList={ManagerAssignmedDropdown}
            />
          }
          show={openModal != false}
          modalClosed={() => setopenModal(false)}
        />
      ) : null}

      {showLoader ? (
        <div className="loader">
          {" "}
          <CircularProgress />
        </div>
      ) : (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            className={classes.table}
            stickyHeader
            aria-label="sticky table"
          >
            <TableHead>
              <TableRow>
                <StyledTableCell className={classes.tableRow}>
                  Project
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setproject((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {project.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setproject((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedProjectList}
                          setDefaultValue={setCheckedProjectList}
                          dropdownClass=" dropdown projectDropdown"
                          wholeList={ProjectDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"name"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell className={classes.tableRow}>
                  Client
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setClient((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {Client.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setClient((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedClientList}
                          setDefaultValue={setCheckedClientList}
                          dropdownClass=" dropdown clientDropdown"
                          wholeList={ClientDropdownlist}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"client"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "8vw" }}
                >
                  Billable
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setBillable((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {Billable.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setBillable((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedBillableList}
                          setDefaultValue={setCheckedBillableList}
                          dropdownClass=" dropdown billableDropdown"
                          wholeList={BillableDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"billable"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "8vw" }}
                >
                  Manager Assigned
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setManagerAssignmed((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {ManagerAssignmed.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setManagerAssignmed((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedManagerAssignList}
                          setDefaultValue={setCheckedManagerAssignList}
                          dropdownClass=" dropdown managerDropdown"
                          wholeList={ManagerAssignmedDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"managerAssigned"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell className={classes.tableRow}>
                  Creation Date
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setcreationDate((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {creationDate.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setcreationDate((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Calender
                          dropdownClass=" dropdown dueDropdown"
                          date={CreationdropdownDate}
                          setDate={setCreationDropdownDate}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          valueKey="creationDate"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "8vw" }}
                >
                  Due Date
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setdueDate((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {dueDate.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setdueDate((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Calender
                          dropdownClass=" dropdown dueDropdown"
                          date={DuedropdownDate}
                          setDate={setDueDropdownDate}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          valueKey="dueDate"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "10vw" }}
                >
                  Closing Date
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setclosingDate((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {closingDate.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setclosingDate((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Calender
                          dropdownClass=" dropdown dueDropdown"
                          date={ClosingdropdownDate}
                          setDate={setClosingDropdownDate}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          valueKey={"closingDate"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell className={classes.tableRow}>
                  Hours Spent
                </StyledTableCell>
                <StyledTableCell className={classes.tableRow}>
                  Cost Spent
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "8vw" }}
                >
                  Status
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setstatus((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {status.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setstatus((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedStatusList}
                          setDefaultValue={setCheckedStatusList}
                          dropdownClass=" dropdown statusDropdown"
                          wholeList={statusDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"status"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ backgroundColor: "white", border: "none" }}
                ></StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows && rows.length > 0 ? (
                rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => (
                    <StyledTableRow key={row.name}>
                      <StyledTableCell component="th" scope="row">
                        {row.project}
                      </StyledTableCell>
                      <StyledTableCell>{row.client}</StyledTableCell>
                      <StyledTableCell>
                        {row.billable === true ? "True" : "False"}
                      </StyledTableCell>
                      <StyledTableCell>{row.manager}</StyledTableCell>
                      <StyledTableCell>
                        {moment(row.creationdate).format("ll")}
                      </StyledTableCell>
                      <StyledTableCell>
                        {moment(row.duedate).format("ll")}
                      </StyledTableCell>
                      <StyledTableCell>{row.closingdate}</StyledTableCell>
                      <StyledTableCell>{row.hoursspent}</StyledTableCell>
                      <StyledTableCell>{row.costspent}</StyledTableCell>
                      <StyledTableCell>
                        {row.status.toUpperCase()}
                      </StyledTableCell>
                      <StyledTableCell style={{ backgroundColor: "white" }}>
                        <EditOutlinedIcon
                          onClick={() => {
                            setShowEditModal(true);
                            setModalValue(row);
                          }}
                          style={{
                            position: "absolute",
                            top: "50%",
                            right: "1%",
                            width: "20px",
                            height: "20px",
                            cursor: "pointer",
                          }}
                        />
                        <DeleteForeverIcon
                          onClick={() => {
                            setShowDeleteModal(true);
                            setModalDeleteValue(row.id);
                          }}
                          style={{
                            position: "absolute",
                            top: "50%",
                            right: "0%",
                            width: "20px",
                            height: "20px",
                            cursor: "pointer",
                          }}
                        />
                      </StyledTableCell>
                    </StyledTableRow>
                  ))
              ) : (
                <div className="NoDataFound">No Data Available</div>
              )}
            </TableBody>
          </Table>
          <TablePagination
            className={classes.pagination}
            rowsPerPageOptions={[5, 10, 15, 20]}
            component="div"
            count={rows ? rows.length : 0}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </TableContainer>
      )}
      {showEditModal ? (
        <Modal
          children={
            <EditProject
              row={modalValue}
              managerList={ManagerAssignmedDropdown}
              modalClosed={() => setShowEditModal(false)}
            />
          }
          show={showEditModal != false}
          modalClosed={() => setShowEditModal(false)}
        />
      ) : null}

      {showDeleteModal ? (
        <Modal
          style={{
            width: "50%",
            left: "30%",
            top: "25%",
            textAlign: "center",
          }}
          children={
            <DeleteProject
              row={modalDeleteValue}
              modalClosed={() => setShowDeleteModal(false)}
            />
          }
          show={showDeleteModal != false}
          modalClosed={() => setShowDeleteModal(false)}
        />
      ) : null}
    </React.Fragment>
  );
}

//null values will be not yet updated
