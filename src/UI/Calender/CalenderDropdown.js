import React from "react";
import { FormControl } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import Notiflix from "notiflix";

const useStyles = makeStyles({
  dropdown: {
    textAlign: "left",
    font: "normal normal 600 12px/17px Open Sans !important",
    letterSpacing: "0px",
    color: "#808080",
    opacity: "1",
    textTransform: "capitalize",
  },
  list: {
    background: "#FFE4C0 0% 0% no-repeat padding-box",
    borderRadius: "0px 10px 10px 10px",
    margin: "7px",
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    width: 150,
    marginLeft: "3%",
    "& label": {
      color: "#000000",
    },
    "& input": {
      fontSize: "12px",
      color: "#808080",
    },
  },
});
function CalenderDropdown(props) {
  const classes = useStyles();
  const { valueKey } = props;
  const [selectedDate, setSelectedDate] = React.useState(props.date);

  const handleDateChangeEven = (e) => {
    props.setDate((prev) => {
      let newObj = [...prev];
      newObj[0] = e.target.value;
      return newObj;
    });
    let newArray;
    setSelectedDate([e.target.value, selectedDate[1]]);
    if (valueKey === "creationDate") {
      newArray =
        props.refdata
         &&
        props.refdata.filter(
          (elem) =>
            moment(elem[props.valueKey]) >=
            moment(moment(e.target.value, "YYYY-MM-DD").toDate())
        );
    } else {
      newArray =
        props.refdata &&
        props.refdata.filter(
          (elem) =>
            moment(moment(elem[valueKey], "YYYY-MM-DD").toDate()) >=
            moment(moment(e.target.value, "YYYY-MM-DD").toDate())
        );
    }
    props.setrefdata(newArray);
  };

  const handleDateChange = (e) => {
    if (
      moment(moment(selectedDate[0], "YYYY-MM-DD").toDate()) >=
      moment(moment(e.target.value, "YYYY-MM-DD").toDate())
    ) {
      Notiflix.Notify.warning("End Date cannot be less than Start Date");
      return;
    } else {
      props.setDate((prev) => {
        let newObj = [...prev];
        newObj[1] = e.target.value;
        return newObj;
      });
      setSelectedDate([selectedDate[0], e.target.value]);
      let newArray;
      if (valueKey === "creationDate") {
        newArray =
          props.refdata &&
          props.refdata.filter(
            (elem) =>
              moment(elem[props.valueKey]) <=
              moment(moment(e.target.value, "YYYY-MM-DD").toDate())
          );
      } else {
        newArray =
          props.refdata &&
          props.refdata.filter(
            (elem) =>
              moment(moment(elem[valueKey], "YYYY-MM-DD").toDate()) <=
              moment(moment(e.target.value, "YYYY-MM-DD").toDate())
          );
      }
      props.setrefdata(newArray);
    }
  };

  return (
    <form className={classes.container} noValidate>
      <div
        className={props.dropdownClass ? props.dropdownClass : classes.dropdown}
      >
        <FormControl style={{ width: "100%" }}>
          <div className={classes.list}>
            <div
              style={{
                textAlign: "center",
                color: "#808080",
                fontSize: "12px",
              }}
            >
              Enter Date
            </div>
            <TextField
              id="date"
              label="From"
              type="date"
              onChange={(e) => handleDateChangeEven(e)}
              value={selectedDate[0]}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <div style={{ margin: "5% 0" }}>
              <TextField
                id="date"
                label="To"
                type="date"
                value={selectedDate[1]}
                onChange={(e) => handleDateChange(e)}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </div>
        </FormControl>
      </div>
    </form>
  );
}

export default CalenderDropdown;
