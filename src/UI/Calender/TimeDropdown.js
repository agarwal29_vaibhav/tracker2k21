import React from "react";
import {
    FormControl,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles({
    dropdown: {
        textAlign: "left",
        font: "normal normal 600 12px/17px Open Sans !important",
        letterSpacing: "0px",
        color: "#808080",
        opacity: "1",
        textTransform: "capitalize",
    },
    list: {
        background: '#FFE4C0 0% 0% no-repeat padding-box',
        borderRadius: '0px 10px 10px 10px',
        margin: '7px'
      },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      textField: {
        width: 150,
        marginLeft:'3%',
        '& label':{
            color:'#000000'
        },
        '& input':{
            fontSize:'12px',
            color:'#808080'
        }
      },
});
function TimeDropdown(props) {
    const classes = useStyles();
    const [selectedDate, setSelectedDate] = React.useState(null);

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
    return (
        <form className={classes.container} noValidate>
        <div className={props.dropdownClass ? props.dropdownClass : classes.dropdown}>
            <FormControl style={{ width: "100%" }}>
                <div className={classes.list}>
                    <div style={{textAlign: 'center', color:'#808080', fontSize: '12px'}}>Enter Hours</div>
                    <TextField
                        id="date"
                        label="From"
                        type="time"
                        defaultValue="00:00"
                        className={classes.textField}
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                   <div style={{margin:'5% 0'}}>
                   <TextField
                        id="date"
                        label="To"
                        type="time"
                        defaultValue="00:00"
                        className={classes.textField}
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                   </div>
                </div>
            </FormControl>
        </div>
    </form>
    );
}

export default TimeDropdown;
