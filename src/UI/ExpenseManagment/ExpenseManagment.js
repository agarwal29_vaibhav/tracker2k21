import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import TablePagination from "@material-ui/core/TablePagination";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Dropdown from "../Dropdown/AddToListDropdown";
import Calender from "../Calender/CalenderDropdown";

import Modal from "../Modal/Modal";
import AddIcon from "@material-ui/icons/Add";
import Header from "../Header/Header";
import CreateExpense from "../../Components/Create/CreateExpense";
import EditExpense from "../../Components/Edit/EditExpense";
import DeleteExpense from "../../Components/Delete/DeleteExpense";
import axios from "axios";
import moment from "moment";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../../App.css";
import GetAppIcon from "@material-ui/icons/GetApp";
import Notiflix from "notiflix";
import downloadIcon from "../../Assets/download.svg";
import { ClickAwayListener } from "@material-ui/core";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#FFF1E1",
    color: "#4E4E4E",
    fontWeight: "600",
  },
  body: {
    fontSize: 14,
    borderBottom: 0,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    position: "relative",
    background: "#F8F8F8 0% 0% no-repeat padding-box",
  },
}))(TableRow);

function createData(
  id,
  name,
  date,
  voucher,
  expenseCat,
  description,
  amount,
  attach,
  status,
  firmname,
  comments
) {
  return {
    id,
    name,
    date,
    voucher,
    expenseCat,
    description,
    amount,
    attach,
    status,
    firmname,
    comments,
  };
}

const useStyles = makeStyles({
  table: {
    width: "100%",
    borderSpacing: "5px",
    borderCollapse: "separate",
  },
  tableContainer: {
    boxShadow: "none",
    minHeight: "68vh",
    maxHeight: "68vh",
    overflowY: "auto",
    width: "100%",
    overflowX: "auto",
    "&::-webkit-scrollbar": {
      width: "10px",
      height: "15px",
      backgroundColor: "white",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#DADADA",
      borderRadius: "10px",
    },
  },
  pagination: {
    bottom: "4rem",
    right: "0",
    width: "40%",
    zIndex: "1",
    position: "absolute",
  },
  tableRow: {
    "&:first-child": {
      borderTopLeftRadius: "4vh",
    },
    "&:nth-last-child(2)": {
      borderTopRightRadius: "4vh",
    },
  },
});

export default function ExpenseManagment() {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const [openModal, setopenModal] = useState(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [name, setname] = useState({ show: false });
  const [ExpenseCat, setExpenseCat] = useState(false);
  const [Amount, setAmount] = useState(false);
  const [FirmName, setFirmName] = useState(false);
  const [Date, setDate] = useState(false);
  const [status, setstatus] = useState(false);
  const [NameDropdown, setNameDropdown] = useState([]);
  const ExpenseCatDropdownlist = [
    "Late sitting food expenses",
    "Partner food expenses",
    "Printer and Paper Rim",
    "Maid & Safaiwala",
    "Repair & Maintance",
    "Other Expenses",
    "Hotel",
    "Cab",
    "Courier",
    "Metro",
    "Tea Expense",
    "Cake Expenses",
    "Other Food Expenses",
    "Other Stationary",
    "Office Consumables",
    "Bike Petrol & Service",
    "Food Expense",
    "Petrol/Diesel",
    "Parking/Toll",
    "Train/Bus",
  ];
  const FirmNameDropdown = ["ARC", "MGSG", "GstConnect"];
  const statusDropdown = ["Open", "Reviewed", "Pending Review"];

  const [RowData, setRowData] = useState([]);
  const [RowDataRef, setRowDataRef] = useState([]);
  const [showEditModal, setShowEditModal] = useState(null);
  const [modalValue, setModalValue] = useState(null);
  const [showDeleteModal, setShowDeleteModal] = useState(null);
  const [modalDeleteValue, setModalDeleteValue] = useState(null);
  const [rows, setRows] = useState(null);
  const [showLoader, setShowLoader] = useState(false);
  const [dropdownDate, setDropdownDate] = useState([null, null]);

  const [checkedNameList, setCheckedNameList] = useState([]);
  const [checkedExpenseList, setCheckedExpenseList] = useState([]);
  const [checkedFirmList, setCheckedFirmList] = useState([]);
  const [checkedStatusList, setCheckedStatusList] = useState([]);

  const attachmentDownload = (attach) => {
    axios
      .get("http://139.59.73.146:9090/v1/api/downloadFile/" + attach)
      .then((response) => {
        window.open(response.config.url, "_blank");
        Notiflix.Notify.success("DOWNLOADED SUCCESSFULLY");
      });
  };

  var loginObj = localStorage.getItem("LoginDetails");
  var loginObject = JSON.parse(loginObj);
  if (!loginObj || loginObj === null) {
    window.location.href = "/";
  }

  useEffect(() => {
    axios
      .get("http://139.59.73.146:9090/v1/api/get/filters/team")
      .then((res) => {
        setNameDropdown(
          res.data.data[0].map((x, index) => {
            return x;
          })
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    setShowLoader(true);
    var loginObj = localStorage.getItem("LoginDetails");
    var loginObject = JSON.parse(loginObj);

    axios
      .post("http://139.59.73.146:9090/v1/api/get/expense", {
        user_name: loginObject.userId,
      })
      .then((res) => {
        setRowData(res.data.data);
        setRowDataRef(res.data.data);
        setShowLoader(false);
      })
      .catch((err) => {
        console.log(err);
        setShowLoader(false);
      });
  }, []);

  useEffect(() => {
    setRows(() => {
      const a = RowData.map((x) => {
        return createData(
          x.id,
          x.name,
          x.date,
          x.voucherNumber == null ? "Yet to be allocated" : x.voucherNumber,
          x.expenseCategory,
          x.description,
          x.amount,
          x.fileName,
          x.status == null ? "NA" : x.status,
          x.firm == null ? "NA" : x.firm,
          x.comments == null ? "NA" : x.comments,
          x.status
        );
      });
      return a;
    });
  }, [RowData]);

  const exportData = () => {
    let data = rows.map((row) => {
      return [
        row.name,
        row.date,
        row.voucher,
        row.expenseCat,
        row.description,
        row.amount,
        row.attach,
        row.firmname,
        row.comments,
        row.status,
      ];
    });

    data.splice(0, 0, [
      "Name",
      "Date",
      "Voucher No.",
      "Expense Category",
      "Description",
      "Amount",
      // "Attach PDF/jpeg of bill",
      "Firm Name",
      "Comments",
      "Status",
    ]);

    let csvContent =
      "data:text/csv;charset=utf-8," + data.map((e) => e.join(",")).join("\n");

    /* create a hidden <a> DOM node and set its download attribute */
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "ExpenseManagement.csv");
    document.body.appendChild(link);
    /* download the data file */
    link.click();
  };

  const checkboxHandler = (e, firmname, Id) => {
    if (e.currentTarget.checked == true) {
      axios
        .post("http://139.59.73.146:9090/v1/api/approve/expense", {
          id: Id,
          firm_name: firmname,
        })
        .then((res) => {
          if (res.data.status == "UPDATED") {
            window.location.reload();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <div>
      <Header userName="user" title="Expense Management" />

      <button onClick={() => setopenModal(true)} className="CreateExpense">
        <AddIcon style={{ color: "black" }} /> Create Expense Management
      </button>

      <button onClick={exportData} className="DownloadBtn">
        <img src={downloadIcon} /> Download
      </button>

      {openModal ? (
        <Modal
          children={
            <CreateExpense
              Expenselist={ExpenseCatDropdownlist}
              modalClosed={openModal}
              setopenModal={setopenModal}
            />
          }
          show={openModal != false}
          modalClosed={() => setopenModal(false)}
        />
      ) : null}

      {showLoader ? (
        <div className="loader">
          {" "}
          <CircularProgress />
        </div>
      ) : (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            className={classes.table}
            stickyHeader
            aria-label="sticky table"
          >
            <TableHead>
              <TableRow>
                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Name
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setname((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {name.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setname((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedNameList}
                          setDefaultValue={setCheckedNameList}
                          dropdownClass=" dropdown projectDropdown"
                          wholeList={NameDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"name"}
                          showInput={true}
                          noDataLabel="No Records Found"
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "10vw" }}
                >
                  Date
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setDate((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {Date.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setDate((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Calender
                          dropdownClass=" dropdown dueDropdown"
                          date={dropdownDate}
                          setDate={setDropdownDate}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          valueKey={"date"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "10vw" }}
                >
                  Voucher No.
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Expense Category
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setExpenseCat((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {ExpenseCat.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setExpenseCat((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedExpenseList}
                          setDefaultValue={setCheckedExpenseList}
                          dropdownClass=" ExpenseCatdropdown clientDropdown"
                          wholeList={ExpenseCatDropdownlist}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"expenseCategory"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Description
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "5vw" }}
                >
                  Amount
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "10vw" }}
                >
                  Firm Name
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setFirmName((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {FirmName.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setFirmName((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedFirmList}
                          setDefaultValue={setCheckedFirmList}
                          dropdownClass=" dropdown managerDropdown"
                          wholeList={FirmNameDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"firm"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "10vw" }}
                >
                  Comments
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ width: "15vw" }}
                >
                  Status
                  <ArrowDropDownIcon
                    style={{
                      position: "absolute",
                      color: "#FF910E",
                      right: "0",
                      top: "32.5%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setstatus((prev) => {
                        return { show: !prev.show };
                      });
                    }}
                  />
                  {status.show ? (
                    <ClickAwayListener
                      onClickAway={() => {
                        setstatus((prev) => {
                          return { show: false };
                        });
                      }}
                    >
                      <div>
                        <Dropdown
                          multiple={true}
                          defaultValue={checkedStatusList}
                          setDefaultValue={setCheckedStatusList}
                          dropdownClass=" dropdown statusDropdown"
                          wholeList={statusDropdown}
                          refdata={RowDataRef}
                          setrefdata={(val) => {
                            setRowData(val);
                            setPage(0);
                          }}
                          type={"status"}
                        />
                      </div>
                    </ClickAwayListener>
                  ) : null}
                </StyledTableCell>

                <StyledTableCell
                  className={classes.tableRow}
                  style={{ backgroundColor: "white", border: "none" }}
                ></StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows && rows.length > 0 ? (
                rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      // {rows.map((row) => (
                      <StyledTableRow>
                        <StyledTableCell component="th" scope="row">
                          {row.name}
                        </StyledTableCell>
                        <StyledTableCell>
                          {moment(row.date).format("ll")}
                        </StyledTableCell>
                        <StyledTableCell>{row.voucher}</StyledTableCell>
                        <StyledTableCell>{row.expenseCat}</StyledTableCell>
                        <StyledTableCell>{row.description}</StyledTableCell>
                        <StyledTableCell>{row.amount}</StyledTableCell>

                        <StyledTableCell>{row.firmname}</StyledTableCell>
                        <StyledTableCell>{row.comments}</StyledTableCell>
                        <StyledTableCell>{row.status}</StyledTableCell>
                        <StyledTableCell style={{ backgroundColor: "white" }}>
                          <EditOutlinedIcon
                            onClick={() => {
                              setShowEditModal(true);
                              setModalValue(row);
                            }}
                            style={{
                              position: "absolute",
                              top: "50%",
                              right: "1.125%",
                              width: "20px",
                              height: "20px",
                              cursor: "pointer",
                            }}
                          />
                          <DeleteForeverIcon
                            onClick={() => {
                              setShowDeleteModal(true);
                              setModalDeleteValue(row.id);
                            }}
                            style={{
                              position: "absolute",
                              top: "50%",
                              right: "0%",
                              width: "20px",
                              height: "20px",
                              cursor: "pointer",
                            }}
                          />
                        </StyledTableCell>
                        <StyledTableCell style={{ backgroundColor: "white" }}>
                          <GetAppIcon
                            style={{ float: "right" }}
                            onClick={() => attachmentDownload(row.attach)}
                            style={{
                              position: "absolute",
                              top: "50%",
                              right: "3.2%",
                              width: "20px",
                              height: "20px",
                              cursor: "pointer",
                            }}
                          />
                          {loginObject.userType == "ADMIN" &&
                          row.status == "REVIEWED" ? (
                            <input
                              type="checkbox"
                              onClick={(e) =>
                                checkboxHandler(e, row.firmname, row.id)
                              }
                              style={{
                                position: "absolute",
                                top: "50%",
                                right: "4.7%",
                                width: "14px",
                                height: "14px",
                                cursor: "pointer",
                              }}
                            />
                          ) : null}
                        </StyledTableCell>
                      </StyledTableRow>
                    );
                  })
              ) : (
                <div className="NoDataFound">No Data Available</div>
              )}
            </TableBody>
          </Table>
          <TablePagination
            className={classes.pagination}
            rowsPerPageOptions={[5, 10, 15, 20]}
            component="div"
            count={rows ? rows.length : 0}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </TableContainer>
      )}
      {showEditModal ? (
        <Modal
          style={{ top: "5%" }}
          children={
            <EditExpense
              row={modalValue}
              Expenselist={ExpenseCatDropdownlist}
              FirmList={FirmNameDropdown}
              modalClosed={() => setShowEditModal(false)}
            />
          }
          show={showEditModal != false}
          modalClosed={() => setShowEditModal(false)}
        />
      ) : null}

      {showDeleteModal ? (
        <Modal
          style={{ width: "50%", left: "30%", top: "25%", textAlign: "center" }}
          children={
            <DeleteExpense
              row={modalDeleteValue}
              modalClosed={() => setShowDeleteModal(false)}
            />
          }
          show={showDeleteModal != false}
          modalClosed={() => setShowDeleteModal(false)}
        />
      ) : null}
    </div>
  );
}
