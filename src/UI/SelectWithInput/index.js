import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import styles from "./index.module.css";
import arabicStyles from "./arabicStyles.module.css";
import "./common.css";
import "./commonArabic.css";

//func for handling filtering in options
const filter = createFilterOptions();

/*
  Here, 
  state=>{
    1.selectedValue state is used to store the value of selected option.
    2.options is the array list of dropdown options.
    3.isConstantAdded is used to tell whether the selected value is from list of options or some constant value
    4.constantValue is used to store the constant value entered
  }
  props=>{
    1.dropdownOptions is the array of options in select optionList.
    2.optionKey -> key which is used to display option from options array
    5.showEmptyString -> whether empty string should be added as option in select
    6.showConstValue -> whether constant should be added as option in select
    7.setIsConstant -> to know whether the selected value is from list of options or some constant value
    8.setValue -> get value of selected option or constant value entered
  }
  */

function SelectWithInput(props) {
  const [selectedValue, setSelectedValue] = useState(null); //-->selectedValue
  const [options, setOptions] = useState([]);
  const [isConstantAdded, setIsConstantAdded] = useState(false);
  const [constantValue, setConstantValue] = useState("");
  let constantOption = "Constant";

  useEffect(() => {
    if (
      props.value ||
      (props.value && props.value === "" && props.showEmptyString)
    ) {
      if (props.isConstant) {
        setSelectedValue(constantOption);
        setIsConstantAdded(true);
        setConstantValue(props.value);
      } else {
        setIsConstantAdded(false);
        setConstantValue("");
        if (props.value && props.value === "") {
          setSelectedValue("");
        } else {
          setSelectedValue(props.value);
        }
      }
    } else {
      setSelectedValue(null);
      setConstantValue("");
      if (props.isConstant) {
        setIsConstantAdded(true);
      } else {
        setIsConstantAdded(false);
      }
    }
  }, [props.value]);

  useEffect(() => {
    let localDropdownArr = props.dropdownOptions
      ? [...props.dropdownOptions]
      : [];
    setOptions(localDropdownArr);
  }, [props.dropdownOptions]);

  console.log(options);

  const Select = () => {
    return (
      <Autocomplete
        value={isConstantAdded ? "" : selectedValue}
        onChange={(e, newValue) => {
          if (newValue && newValue.inputValue && props.showConstValue) {
            // Create a new value from the user input
            setConstantValue(newValue.inputValue);
            setIsConstantAdded(true);
            setSelectedValue(constantOption);
          } else {
            if (newValue === constantOption && props.showConstValue) {
              setIsConstantAdded(true);
              setConstantValue("");
            } else {
              setIsConstantAdded(false);
              setSelectedValue(newValue);
              setConstantValue("");
            }
          }
        }}
        filterOptions={(filterOptions, params) => {
          const filtered = filter(filterOptions, params);
          // Suggest the creation of a new value
          if (params.inputValue !== "" && props.showConstValue) {
            filtered.push({
              inputValue: params.inputValue,
              title: params.inputValue,
            });
          }
          return filtered;
        }}
        className={
          props.selectWithInput ? props.selectWithInput : styles.selectWithInput
        }
        options={options}
        getOptionLabel={(option) => {
          // Value selected with enter, right from the input
          if (typeof option === "string") {
            //unused
            return option;
          }
          // Add "xxx" option created dynamically
          if (option.inputValue && props.showConstValue) {
            return option.inputValue;
          }

          if (option === constantOption && props.showConstValue) {
            return "";
          }
          // Regular option
          return option;
        }}
        disableClearable
        renderOption={(option) => {
          return (
            <div className={styles.selectWithInputOptions}>
              {option.inputValue && props.showConstValue ? (
                <div className={styles.AddToConst}>
                  <p>{option.inputValue}</p>
                  <p>Add as Client</p>
                </div>
              ) : (
                <span
                  style={
                    option === constantOption ? { ...props.optionStyles } : {}
                  }
                >
                  {option}
                </span>
              )}
            </div>
          );
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            className={`${styles.selectWithInputTextField} selectWithInputTextField`}
            variant="outlined"
          />
        )}
      />
    );
  };

  useEffect(() => {
    if (isConstantAdded && props.showConstValue) {
      if (constantValue !== props.value) {
        //set focus on the constant input field
        document
          .getElementById(`input_with_select_${props.id ? props.id : null}`)
          .focus();
        //if value is constant, then set constant value to props.setValue and true to props.setIsConstant
        props.setValue(constantValue);
        props.setIsConstant && props.setIsConstant(true);
      }
    } else {
      //if value is not constant, then set selected option value to props.setValue and false to props.setIsConstant
      if (
        selectedValue !== props.value &&
        selectedValue !== constantOption &&
        selectedValue
      ) {
        props.setValue(selectedValue);
        props.setIsConstant && props.setIsConstant(false);
      }
    }
  }, [isConstantAdded, selectedValue, constantValue]);

  return (
    <div>
      {(props.showConstValue &&
        selectedValue &&
        selectedValue === constantOption) ||
      isConstantAdded ? (
        //code rendered when constant value is to be entered
        <div className="relative">
          {!props.isConstantIcon && (
            <span className={styles.constantIcon}>C</span>
          )}
          <input
            id={`input_with_select_${props.id ? props.id : null}`}
            autofocus
            value={constantValue}
            className={styles.multiSelectConstInput}
            onChange={(e) => setConstantValue(e.target.value)}
          />
          {Select()}
        </div>
      ) : (
        Select()
      )}
    </div>
  );
}

export default SelectWithInput;
